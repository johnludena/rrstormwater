<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rrstormwater' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',$@v&n|#WeLc2u{CjF/$/|.*xno#si:i<#u89_ks?xcUxz}*!Zu?hsGo}8aM|1K#' );
define( 'SECURE_AUTH_KEY',  '_p90Lg&>?]8kitpr/l<IuDQDdbto=Ky7y1tN3aJl~+W.9u-eMS=/l;:ZGVz(szw5' );
define( 'LOGGED_IN_KEY',    'g/y{JBf ^( A9kHg$_0bKdA}-o5<mmmPYAQos|27Whn/R:umNC-Z2ZC.z3<H#k[x' );
define( 'NONCE_KEY',        'afo2e5~hM^iPWcJ1>]myCK!Zh89lZ]v,X]mt{ke+I8J1SV,`=d/1d#Y9YzZeN_JP' );
define( 'AUTH_SALT',        'XMqne*<r#jmEgzzN!l[06Z!568-h*u{* 6pGz1u@XOcw%Bak97x[MS`VEf[uA`r%' );
define( 'SECURE_AUTH_SALT', '<?@J;GXhS^XGbt|Itw!3d>7V|Vx~a a9a{AJIg*!lWf~=BRMmPR<HAJxhP,[61Q3' );
define( 'LOGGED_IN_SALT',   '33n{tDO4|y;E;l5c|fFj1]8KJByUT_tS~QKxCdY[dM+Odg8S&/eLxO`x+XRv6_yb' );
define( 'NONCE_SALT',       'D1NEqH@fZ_$|8Ds%=g2(i=f@tN/%{Hs0 0:f9so)|+(CI-3;jma+ M8=Z ;).,^0' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
