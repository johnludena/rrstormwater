<?php /* Template Name: Subpage w/Header+Sidenav */ ?>
<?php get_header(); ?>
         


<div id="breadcrumbContainer">

        <div class="container" >
                <div class="row">
                        <div class="col">
                                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                                    <?php if(function_exists('bcn_display'))
                                    {
                                        bcn_display();
                                    }?>
                                </div>                  
                        </div>
                </div>
        </div>
        
</div>


    <section id="hero-banner">
        <div class="royalSlider royalSlider--hero rsDefault">

                        <div class="rsContent">
                            <img src="<?php the_field('banner_image'); ?>" />

                            <div class="slide-text">
                                <div class="inner-text-wrapper">
                                    <h1><?php the_field('banner_title'); ?></h1>
                                    <p><?php the_field('banner_description'); ?></p>
                                </div>

                            </div>
                        </div>
        </div>
    </section>


<div class="container" id="subpageContainer">
        <div class="row">
                <div class="col-lg-2">
                        
                        <ul id="sidenav">



<?php

                        $args = array(
                            'post_type'      => 'page',
                            'posts_per_page' => -1,
                            'post_parent'    => $post->ID,
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                         );


                        $parent = new WP_Query( $args );

                        if ( $parent->have_posts() ) : ?>

                            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                                <div id="parent-<?php the_ID(); ?>" class="parent-page">

                                    <li><a class="<?php if($parent_page_id == $postID){echo "active";}; ?>" href="<?php the_permalink(); ?>"><?php printf( '%1$s', get_the_title() );  ?></a></li>

                                </div>

                            <?php endwhile; ?>

                        <?php endif; wp_reset_postdata(); ?>

                        </ul>

                </div>
                <div class="col-lg-10">
                        <div class="row">
                                <div class="col-lg-12">
                                        <h1><?php echo get_the_title(); ?></h1>
                                        <p class="lead-paragraph"><?php echo get_field( "lead_paragraph" ); ?></p>
                                        <h2><?php echo get_field( "first_title" ); ?></h2>
                                        <p><?php echo get_field( "first_title_desc" ); ?></p>
                                </div>
                        </div>


                    <?php
                    $career_card = get_field('card_repeater', get_the_ID());
                    $count = count($career_card);

                    if (!$career_card) {
                        return;
                    }
                    ?>

                    <div class="row">
                        <?php foreach ($career_card as $item):

                            if ($count == 1) {
                                $class = 'col-lg-12';
                            } elseif ($count == 2) {
                                $class = 'col-lg-6';
                            } else {
                                $class = 'col-lg-4';
                            }

                            $icon = $item['card_icon'];
                            $title = $item['card_title'];
                            $card_desc = $item['card_desc'];

                            ?>
                            <div class="<?php echo $class; ?> text-center card-body d-flex align-items-stretch">
                                <div class="padding-50 bg-gray-light">
                                    <span class="careers-icon" data-aos="fade-up" data-aos-duration="600"><?php echo $icon; ?></span>
                                    <h3 class="padding-top-bottom-25" data-aos="fade-up" data-aos-duration="800"><?php echo $title ?></h3>
                                    <p data-aos="fade-up" data-aos-duration="1000"><?php echo $card_desc; ?></p>
                                </div>
                            </div>

                        <?php endforeach; ?>



                    </div>



                        <div class="row padding-top-50">
                                <div class="col-lg-12">
                                        <h2><?php echo get_field( "second_title" ); ?></h2>
                                        <p><?php echo get_field( "second_title_desc" ); ?></p>
                                </div>
                        </div>


                    <?php
                    $career_box = get_field('second_card_repeater', get_the_ID());
                    $count = count($career_box);

                    if (!$career_box) {
                        return;
                    }
                    ?>

                    <div class="row padding-bottom-50">
                        <?php foreach ($career_box as $item):

                            if ($count == 1) {
                                $class = 'col-lg-12';
                            } elseif ($count == 2) {
                                $class = 'col-lg-6';
                            } else {
                                $class = 'col-lg-6';
                            }

                            $title = $item['card_title'];
                            $card_desc = $item['card_desc'];

                            ?>
                            <div class="<?php echo $class; ?> text-center card-body d-flex align-items-stretch" data-aos="flip-up" data-aos-duration="400">
                                <div class="padding-25 about-border">
                                    <h3><?php echo $title ?></h3>
                                    <p><?php echo $card_desc; ?></p>
                                </div>
                            </div>

                        <?php endforeach; ?>



                    </div>


                        <div class="row addContList">
                        <?php

                        // check if the repeater field has rows of data
                        if( have_rows('image_card') ):

                                // loop through the rows of data
                            while ( have_rows('image_card') ) : the_row();
                                ?>


                                <div class="col-lg-4">
                                        <img src="<?php the_sub_field('image_card_image'); ?>" alt="" class="img-fluid">
                                        <h3><?php the_sub_field('image_card_title'); ?></h3>
                                        <p><?php the_sub_field('image_card_description'); ?></p>
                                </div>
                                
                                
                                <?php 
                            endwhile;

                        else :

                            // no rows found

                        endif;

                        ?>

                                


                        </div>                  
                </div>
        </div>

</div>





</div><!-- .content-area -->

<?php get_footer(); ?>