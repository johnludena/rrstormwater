<?php
/*
 * Template Name: Compliance
 * Template Post Type: compliance_item
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>


    <section id="our-insights" class="padding-top-60">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">

                <h2 class="heading-bottom-border"><?php the_title(); ?></h2>
                <p class="lead-paragraph"><?php the_field('text_intro'); ?></p><br/>

            </div>
        </div>
    </div>
</section>

    <section id="contact-form" class="padding-top-bottom-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8 padding-bottom-60">
                    <?php echo get_post_field('post_content', $post->ID); ?>
                </div>
            </div>
        </div>
    </section>


    <section id="compliance" class="padding-top-60">
        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8">
                        <p class="text-white font-size-36"><strong>Need help with a project?</strong><br>
                        Contact our team to get started today!</p>
                    </div>
                    <div class="col-auto mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" href="#">Request a consultation</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>