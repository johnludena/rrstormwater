<footer class="bg-blue-dark overflow-hidden padding-top-bottom-60 m-padding-top-bottom-30">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-6">
                <div class="d-flex justify-content-center footer-logo-mobile margin-bottom-30">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo-light.svg" alt="R+R Stormwater logo"></a>
                </div>

                <p class="text-white font-size-28 m-font-size-22">
                    <a class="text-blue-light" href="<?php echo esc_url( home_url( '/' ) ); ?>receive-an-nov/">Receive a NOV?</a> We can help.<br>
                    Contact us at <a class="text-white" href="tel:<?php the_field('telephone_number', 'option'); ?>"><?php the_field('telephone_number', 'option'); ?></a>.
                </p>
                
                <p class="text-white roboto-light margin-top-40 m-margin-top-30 m-font-size-14">
                    <span itemscope itemtype="http://schema.org/Organization">
                        <?php $address = get_field('address', 'option'); ?>
                        <span itemprop="name"><strong><?php echo $address['company_title']; ?></strong></span><br />
                        <span itemprop="streetAddress"><?php echo $address['physical_address']; ?></span><br />
                         <span itemprop="addressRegion"><?php echo $address['city']; ?></span>, <span itemprop="addressLocality"><?php echo $address['state']; ?></span>, <span itemprop="postalCode"><?php echo $address['zip_code']; ?></span>
                    </span>
                </p>
            </div>

            <div class="col-lg-6">
                <div class="d-flex justify-content-end footer-logo-desktop">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo-light.svg" alt="R+R Stormwater logo"></a>
                </div>

                <div class="d-flex justify-content-end align-items-center margin-top-100 m-margin-top-30 social-icons-container">
                    <span class="text-white">Follow us </span>
                    <ul id="social-icons" class="list-unstyled d-flex font-size-22">

                        <?php $socialMedia = get_field('social_media', 'option'); ?>

                        <?php if( $socialMedia['display_linkedin'] ) : ?>
                            <li><a href="<?php echo $socialMedia['linkedin_url']; ?>" target="_blank"><i class="text-blue-light fab fa-linkedin-in"></i></a></li>
                        <?php endif; ?>

                        <?php if( $socialMedia['display_facebook'] ) : ?>
                            <li><a href="<?php echo $socialMedia['facebook_url']; ?>" target="_blank"><i class="text-blue-light fab fa-facebook-f"></i></a></li>
                        <?php endif; ?>

                        <?php if( $socialMedia['display_twitter'] ) : ?>
                            <li><a href="<?php echo $socialMedia['twitter_url']; ?>" target="_blank"><i class="text-blue-light fab fa-twitter"></i></a></li>
                        <?php endif; ?>

                        <?php if( $socialMedia['display_youtube'] ) : ?>
                            <li><a href="<?php echo $socialMedia['youtube_url']; ?>" target="_blank"><i class="text-blue-light fab fa-youtube"></i></a></li>
                        <?php endif; ?>

                        <li><a href="<?php bloginfo('template_url'); ?>/feed/" target="_blank"><i class="text-blue-light fas fa-rss"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <hr>

        <div class="row justify-content-between">
            <div class="col">
                <div class="d-flex justify-content-between font-size-14 m-font-size-12 footer-meta">
                    <p class="text-white roboto-light">&copy; Copyright <?php echo date('Y'); ?> <?php the_field('copyright_statement', 'option'); ?></p>
                    <?php wp_nav_menu(
						array(
							'theme_location' => 'footer-links',
							'menu_class' => 'd-flex flex-row roboto-light navbar-nav'
						)); ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#top" class="back-to-top js-back-to-top"><i class="fas fa-chevron-up"></i></a>


<?php wp_footer(); ?>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119190162-1"></script>
<script> 
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-119190162-1');
</script>
</body>
</html>