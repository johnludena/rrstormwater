<?php /* Template Name: Compliance - Detail Page */ ?>
<?php get_header(); ?>

<?php while( have_posts() ) : the_post(); get_template_part( 'template-parts/content', 'page' ); if( comments_open() || get_comments_number() ) {
        comments_template();
} endwhile; ?>

<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
    <div class="row">
        <div class="col-lg-2">
            <?php include('inc/navigation-side.php'); ?>
        </div>

        <div class="col-lg-10">
            <div class="row">
                <div class="col">
                    <h1><?php echo get_the_title(); ?></h1>

                    <?php if( get_field('lead_paragraph') ) : ?>
                    <p class="lead-paragraph"><?php echo get_field('lead_paragraph'); ?></p>
                    <?php endif; ?>

                    <?php if( get_field('lead_image_title') ) : ?>
                    <h2><?php echo get_field('lead_image_title'); ?></h2>
                    <?php endif; ?>

                    <?php if( get_field('lead_image') ) : ?>
                    <img src="<?php echo get_field('lead_image'); ?>" alt="">
                    <?php endif; ?>

                    <?php echo get_field('body_content'); ?>
                    
<!-- column layout for states -->


                            <?php $terms = get_terms( array(
                            'taxonomy' => 'state',
                            'hide_empty' => false,
                        )); ?>

                            <div class="row padding-top-30 padding-bottom-100">
                                <?php foreach ($terms as $term):

                                    ?>
                                    <div class="col-lg-4 col-md-6 col-sm-12 padding-15">
                                        <p class="m-0" data-aos="fade-up" data-aos-duration="800"><a href="<?php echo site_url(); ?>/state/<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></p>
                                    </div>

                                <?php endforeach; ?>

                            </div>


<!-- end columns -->




<!--                     <div class="statesList">
                        <?php $terms = get_terms( array(
                            'taxonomy' => 'state',
                            'hide_empty' => false,
                        )); ?>
                        
                        <div class="row">
                            <div class="col-lg-4">
                                <ul class="list-unstyled margin-top-30">
                                <?php foreach( $terms as $term ) { ?>
                                    <li>
                                        <a href="<?php echo site_url(); ?>/state/<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="row addContList">
                <?php if( have_rows('image_card') ): while ( have_rows('image_card') ) : the_row(); ?>
                <div class="col-lg-4">
                    <div class="card-image">
                        <img src="<?php the_sub_field('image_card_image'); ?>" alt="">
                    </div>
                    <div class="card-body">
                        <h3><?php the_sub_field('image_card_title'); ?></h3>
                        <p><?php the_sub_field('image_card_description'); ?></p>
                    </div>      
                </div>
                <?php endwhile; endif; ?>
            </div>                  
        </div>
    </div>
</div>

<?php include('inc/compliance-footer-without-section.php'); ?>


<?php get_footer(); ?>