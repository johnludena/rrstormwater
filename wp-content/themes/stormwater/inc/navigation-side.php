<?php if( $post->post_parent ) {
    $children = wp_list_pages( array(
        'title_li' => '',
        'child_of' => $post->post_parent,
        'echo'     => 0
    ) );
} else {
    $children = wp_list_pages( array(
        'title_li' => '',
        'child_of' => $post->ID,
        'echo'     => 0
    ) );
} if( $children ) : ?>

<button class="mobile-sidenav-trigger js-mobile-sidenav-trigger">
    Navigate <span><?php echo get_the_title(); ?></span> <i class="fas fa-chevron-down"></i>
</button>

<ul id="sidenav" class="list-unstyled" role="navigation">
    <?php echo $children; ?>
</ul>

<?php endif; ?>