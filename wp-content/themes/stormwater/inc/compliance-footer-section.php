    <section id="compliance" class="padding-top-60 m-padding-top-30">
        <div class="container">
            <div class="row m-margin-left-right-15">
                <div class="col-lg-6 justify-content-center">
                    <div class="card card--insights card--large">
                        <!-- Show featured image if available, if not, show placeholder -->
                        <?php $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full'); ?>

                        <?php
                            // Get our variables first
                            $compliance_button_label = get_field('compliance_button_label', 76);
                            $compliance_button_url = get_field('compliance_button_url', 76);
                            $compliance_title = get_field('compliance_title', 76);
                            $compliance_content = get_field('compliance_content', 76);
                            $compliance_image = get_field('compliance_featured_image', 76);
                        ?>

                        <?php if( !empty($compliance_image) ): ?>
                            <div class="card-img-container">
                                <a href="<?php echo $compliance_button_url; ?>">
                                    <img src="<?php echo $compliance_image['url']; ?>" class="card-img-top" alt="" />
                                </a>
                            </div>
                        <?php else: ?>
                            <div class="card-img-container">
                                <a href="<?php echo $compliance_button_url; ?>">
                                    <img src="https://picsum.photos/680/350" class="card-img-top" alt="">
                                </a>
                            </div>

                        <?php endif ?>
                        <div class="card-heading card-body bg-gray-light">
                            <p class="card-category">Regulatory Compliance</p>

                            <h5 class="card-title">
                                <a href="<?php echo $compliance_button_url; ?>"><?php echo $compliance_title; ?></a>

                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="js-eqheight-card-body">
                                <?php echo $compliance_content; ?>
                            </div>

                            <a class="section-cta caret-right" href="<?php echo $compliance_button_url; ?>">
                                <?php echo $compliance_button_label; ?>
                            </a>    
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 justify-content-center m-padding-top-30">
                    <div id="featured" class="card card--insights card--large">
                    <?php global $post;
                        $args = array( 'category' => '79',
                            'posts_per_page' => 1
                        );
                        $myposts = get_posts( $args ); foreach( $myposts as $post ) :  setup_postdata($post); ?>

                        <!-- Show featured image if available, if not, show placeholder -->
                        <?php $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');

                        if( !empty($featured_img_url) ): ?>
                        <div class="card-img-container">
                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php echo $featured_img_url; ?>" class="card-img-top" alt="" />
                            </a>
                        </div>
                        <?php else: ?>
                        <div class="card-img-container">
                            <a href="<?php the_permalink(); ?>">
                                <img src="https://picsum.photos/680/350" class="card-img-top" alt="">
                            </a>
                        </div>

                        <?php endif ?>

                        <div class="card-heading card-body bg-gray-light">
                            <p class="card-category">Featured Insight</p>
                            <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        </div>
                        <div class="card-body h-100">
                            <div class="js-eqheight-card-body">
                                <?php the_excerpt(); ?>
                            </div>

                            <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                        </div>
                        <?php endforeach; wp_reset_query(); ?> 
                    </div>
                </div>

            </div>
        </div>

        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8 col-md-12 col-sm-12 ">
                        <p class="text-white font-size-36 m-font-size-22" data-aos="fade-up" data-aos-duration="800"><strong><?php echo get_field('need_help_title'); ?></strong><br>
                        <?php echo get_field('need_help_description'); ?></p>
                    </div>
                    <div class="col-auto m-mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" href="<?php echo get_field('need_help_button_url'); ?>" aos-init aos-animate data-aos="zoom-in" data-aos-duration="1000"><?php echo get_field('need_help_button_label'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>