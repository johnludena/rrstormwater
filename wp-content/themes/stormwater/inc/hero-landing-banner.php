    <section id="hero-banner">
        <div class="royalSlider royalSlider--hero rsDefault">

                        <div class="rsContent">
                            <img class="mobile-banner" src="<?php the_field('banner_image'); ?>" />

                            <div class="slide-text">
                                <div class="inner-text-wrapper">
                                    <h1><?php the_field('banner_title'); ?></h1>
                                    <p><?php the_field('banner_description'); ?></p>
                                </div>

                            </div>
                        </div>
        </div>
    </section>