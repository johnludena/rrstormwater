    <section id="compliance" class="padding-top-60 m-padding-top-30">
        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8 col-md-12 col-sm-12 ">
                        <p class="text-white font-size-36 m-font-size-22" data-aos="fade-up" data-aos-duration="800"><strong><?php echo get_field('need_help_title'); ?></strong><br>
                        <?php echo get_field('need_help_description'); ?></p>
                    </div>
                    <div class="col-auto mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" href="<?php echo get_field('need_help_button_url'); ?>" aos-init aos-animate data-aos="zoom-in" data-aos-duration="1000"><?php echo get_field('need_help_button_label'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>