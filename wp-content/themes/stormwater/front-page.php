<?php get_header(); ?>

    <section id="hero-banner">
        <div class="royalSlider royalSlider--hero rsDefault">
            <?php if( have_rows('banner_info') ): while( have_rows('banner_info') ): the_row();
                // display a sub field value
                $slide_title = get_sub_field('banner_header');
                $slide_text = get_sub_field('banner_description');
                $slide_img = get_sub_field('banner_image');
                $button_label = get_sub_field('button_label');
                $button_url = get_sub_field('button_url');
                // var_dump($slide_title);
            ?>

            <div class="rsContent">
                <img class="rsImg" src="<?php echo $slide_img; ?>" alt="">

                <div class="slide-text">
                    <div class="rsABlock">
                        <div class="inner-text-wrapper">
                            <h1><?php echo $slide_title; ?></h1>
                            <p><?php echo $slide_text; ?></p>
                            <a class="btn btn-info btn-lg" href="<?php echo $button_url; ?>"><?php echo $button_label; ?></a>
                        </div>
                    </div>
                </div>
            </div>

            <?php endwhile; endif; ?>
        </div>
    </section>
    
<!-- Services section -->
    <!-- start pills -->
    <section id="our-services" class="padding-top-60 m-padding-top-30">
        <div class="container">
            <div class="row justify-content-center padding-bottom-30">
                <div class="col-lg-8 text-center">
                    <h2 class="heading-bottom-border m-font-size-30"><?php the_field('services_title'); ?></h2>
                    <p aos-init aos-animate data-aos="zoom-out" data-aos-duration="800"><?php the_field('services_description'); ?></p>
                </div>
            </div>

            <div class="row no-gutters margin-top-bottom-40 home-services-tabs dark-bg-slide" id="services-tabs">
                <div class="col-lg-3 bg-blue-dark">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <?php
                        $arr = array();
                        // send WP query to get all matching "Services" post types
                        $services_query_args = array(
                            'post_type' => 'service',
                        );
                        $services = new WP_Query($services_query_args); if ($services->have_posts()) : while ($services->have_posts()) : $services->the_post();
                        if($services->posts){                            
                            $arr = $services->posts;
                        }
                        endwhile; wp_reset_postdata(); else : esc_html_e('No testimonials in the diving taxonomy!', 'text-domain'); endif;
                        $reversed = array_reverse($arr);
                        foreach ($reversed as $key => $value) {
                            // get post title in a machine name friendly format (e.g. 'stormwater-management')
                            // to use as ID for matching bootstrap tab functionality
                            $post_title_id = sanitize_title($value->post_title); ?>
                            <a class="nav-link" id="v-pills-<?php echo $post_title_id; ?>-tab" data-toggle="pill" href="#v-pills-<?php echo $post_title_id; ?>" role="tab" aria-controls="v-pills-<?php echo $post_title_id; ?>" aria-selected="true">
                                <?php print($value->post_title); ?>
                            </a>
                        <?php
                        }
                        ?>                            
                    </div>
                </div>
                <div class="col-lg-9 overflow-hidden bg-black">
                    <div class="tab-content" id="v-pills-tabContent">
                        <?php if ($services->have_posts()) : while ($services->have_posts()) : $services->the_post();

                        // get post title in a machine name friendly format (e.g. 'stormwater-management')
                        // to use as ID for matching bootstrap tab functionality
                        $post_title_id = sanitize_title(get_the_title());

                        // TODO: update this to whatever field we end up using for tab's services description
                        // on homepage
                        $service_description = get_field('homepage_summary', $services->ID);

                        // get each "Service" featured image
                        $service_thumb_id = get_post_thumbnail_id();
                        $service_thumb_url_array = wp_get_attachment_image_src($service_thumb_id, 'thumbnail-size', true);
                        $service_thumb_url = $service_thumb_url_array[0];

                        ?>

                        <div class="tab-pane fade" id="v-pills-<?php echo $post_title_id; ?>" role="tabpanel" aria-labelledby="v-pills-<?php echo $post_title_id; ?>-tab">
                            <img src="<?php echo $service_thumb_url; ?>" alt="">
                            <div class="service-description">
                                <h3 class="text-white font-size-22 margin-bottom-20"><?php the_title(); ?></h3>
                                <p class="text-white margin-bottom-15"><?php echo $service_description; ?></p>
                                <a class="text-white section-cta caret-right" href="<?php echo get_permalink(); ?>">Learn more</a>
                            </div>
                        </div>

                        <?php endwhile; wp_reset_postdata(); else : esc_html_e('No testimonials in the diving taxonomy!', 'text-domain'); endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /end pills -->

    <!-- mobile services slider -->
    <section id="mobile-services-slider" class="mobile-only">
        <div class="royalSlider royalSlider--hero royalSlider--services rsDefault">
            <?php $args = array(
                'post_type'      => 'service',
                'posts_per_page' => -1,
                'order'          => 'DESC',
                'orderby'        => 'meta_value'
            );
            
            $parent = new WP_Query( $args ); if( $parent->have_posts() ) : while ( $parent->have_posts() ) : $parent->the_post(); ?>

            <div class="rsContent">
                <img class="rsImg" src="<?php echo the_field('lead_image') ?>" alt="">
                <div class="slide-text dark-bg-slide">
                    <div class="rsABlock">
                        <div class="inner-text-wrapper text-center">
                            <h3 class="text-white"><?php the_title(); ?></h3>
                            <p><?php echo the_field('homepage_summary'); ?></p>
                            <a class="btn btn-info btn-lg" href="<?php echo get_permalink(); ?>">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; endif; wp_reset_query(); ?> 
        </div>
    </section>
    <!-- /end mobile services slider -->
    <!-- /end services section -->

    <section id="case-study" class="padding-top-bottom-40 m-padding-top-bottom-30">
        <div class="container">
            <div class="row d-flex align-items-center padding-top-bottom-30 m-padding-top-bottom-0">
                <div class="col-lg-6">
                    <h2 class="m-font-size-26 border-title" data-aos="fade-up" data-aos-duration="600"><span class="left-blue-border"></span>Proven Success</h2>
                    <p class="m-0 font-size-30 m-font-size-20" data-aos="fade-up" data-aos-duration="800">
                        <?php $client = get_field('client_name', 120); 
                            if(isset($client)){
                                print($client); 
                            } ?>
                    </p>
                    <p class="font-size-18" data-aos="fade-up" data-aos-duration="1000">
                        <?php
                        $location = get_field('location', 120); 
                            if(isset($location)){
                                print($location); 
                            }
                        
                        $market = get_field('market_type', 120); 
                            if(isset($market) && !empty($market)){
                                print(' | '.get_the_category_by_ID($market));   
                            }

                        $service = get_field('service_type', 120); 
                            if($service->post_title){
                                print(' | '.$service->post_title); 
                            }

                        ?>
                    </p>
                    <p data-aos="fade-up" data-aos-duration="1200">Stormwater control measures (SCM) are engineered to treat and control stormwater runoff from developed sites. To protect your investment and keep these systems performing as designed, we offer comprehensive services for all of the most common SCM practices.</p>
                    <p data-aos="fade-up" data-aos-duration="1400">
                        <a class="section-cta caret-right" href="#">View Case Study</a>
                    </p>

                </div>
                <div class="col-lg-6 m-padding-top-30">
                    <div class="twentytwenty-container">
                        <img src="http://staging.adcetera.com/rrstormwater/wp-content/uploads/2019/07/rrstorm-before.png"/>
                        <img src="http://staging.adcetera.com/rrstormwater/wp-content/uploads/2019/07/rrstorm-after.png"/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="violation-notice" class="padding-top-100 padding-bottom-130 m-padding-top-0 padding-bottom-90">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 justify-content-center text-center padding-top-bottom-60 bg-nov">
                    <h2 class="m-font-size-30" aos-init aos-animate data-aos="zoom-out" data-aos-duration="800"><?php the_field('notice_title'); ?></h2>
                    <p class="notice-txt"  aos-init aos-animate data-aos="zoom-out" data-aos-duration="1000"><?php the_field('notice_description'); ?></p>
                    <a href="<?php the_field('notice_button_url'); ?>" class="btn btn-outline-info" aos-init aos-animate data-aos="zoom-out" data-aos-duration="1200">
                        <?php the_field('notice_button_label'); ?>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="our-reach">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 justify-content-center">

                    <div class="row">
                        <div class="col-lg-6 our-reach-bg border-radius-5 m-border-radius-0">
                            <div class="padding-top-bottom-25 padding-left-right-10 m-padding-left-right-0">
                                <h3><?php the_field('our_reach_title'); ?></h3>
                                <p><?php the_field('our_reach_description'); ?></p>
                            </div>

                            <?php $our_reach = get_field('stats_repeater', get_the_ID()); $count = count($our_reach);
                                if(!$our_reach) {
                                    return;
                                } ?>

                            <div class="row our-reach-stats-bg">
                                <?php foreach ($our_reach as $item) :

                                    if ($count == 1) {
                                        $class = 'col-lg-12';
                                    } elseif ($count == 2) {
                                        $class = 'col-lg-6';
                                    } else {
                                        $class = 'col-lg-6';
                                    }

                                    $number = $item['stats_number'];
                                    $number_symbol = $item['stats_symbol'];
                                    $number_desc = $item['stats_description'];

                                    ?>
                                    <div class="<?php echo $class; ?> text-center padding-25">
                                        <p class="stats-num m-0" data-aos="fade-up" data-aos-duration="800">
                                            <span class="js-stats-num-a"><?php echo $number; ?></span><span class="text-blue-base"><?php echo $number_symbol; ?></span>
                                        </p>
                                        <p class="stats-desc" data-aos="fade-up" data-aos-duration="1000">
                                            <?php echo $number_desc; ?>
                                        </p>
                                    </div>

                                <?php endforeach; ?>

                            </div>
                            <div class="padding-top-bottom-25 padding-left-right-10 m-padding-left-right-0">
                                <a class="section-cta caret-right text-blue-dark" href="<?php the_field('stats_button_url'); ?>">
                                    <?php the_field('stats_button_label'); ?>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-6 our-markets m-padding-0">
                            <div class="padding-25 m-padding-left-right-15 bg-blue-dark border-radius-5 m-border-radius-0">
                                <h3 class="text-white"><?php the_field('our_markets_title'); ?></h3>
                                <p class="text-white"><?php the_field('our_markets_description'); ?></p>

                                <div class="row row--market-icons no-gutters ">
                                    <?php $taxonomy_terms = get_terms( array(
                                        'taxonomy' => 'markets',
                                        'hide_empty' => false,
                                    ));

                                    foreach($taxonomy_terms as $taxonomy) {
                                        $taxonomy_icon = get_field('market_icon',$taxonomy->taxonomy.'_'.$taxonomy->term_id ); ?>

                                    <div class="col-lg-4 col-md-4">
                                        <div class="d-flex align-items-center market-icon" data-aos="fade-up" data-aos-duration="1400">
                                            <?php echo $taxonomy_icon; ?>
                                            <p class="text-white"><?php echo $taxonomy->name; ?></p>
                                        </div>
                                    </div>
                                        
                                    <?php } ?>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p><a class="section-cta caret-right" href="<?php the_field('our_markets_button_url'); ?>"><?php the_field('our_markets_button_label'); ?></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 justify-content-center padding-top-bottom-60 m-padding-top-30 m-padding-bottom-0">

                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center padding-top-bottom-30 m-padding-top-0">
                            <h2 class="heading-bottom-border m-font-size-30"><?php the_field('about_us_title'); ?></h2>
                            <p aos-init aos-animate data-aos="zoom-out" data-aos-duration="800"><?php the_field('about_us_descriptions'); ?></p>
                        </div>
                    </div>

                    <?php
                    $about_us = get_field('about_us_repeater', get_the_ID());
                    $count = count($about_us);

                    if (!$about_us) {
                        return;
                    } ?>

                    <div class="row">
                        <?php foreach ($about_us as $item):

                            if ($count == 1) {
                                $class = 'col-lg-12';
                            } elseif ($count == 2) {
                                $class = 'col-lg-6';
                            } else {
                                $class = 'col-lg-4 col-md-4';
                            }

                            $title = $item['about_us_stats_title'];
                            $description = $item['about_us_stats_description'];
                            $number = $item['about_us_stats_number'];
                            $number_desc = $item['about_us_stats_number_description'];

                        ?>
                        <div class="<?php echo $class; ?> text-center m-padding-10" data-aos="flip-up" data-aos-duration="400">
                            <div class="about-border padding-25">
                                <h3 class="m-font-size-18"><?php echo $title; ?></h3>
                                <p class="m-0"><?php echo $description ?></p>
                                <p class="stats-num js-stats-num-b m-0"><?php echo $number; ?></p>
                                <p class="stats-desc m-font-size-14"><?php echo $number_desc; ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="m-5 text-center">
                                <a href="<?php the_field('about_us_button_url'); ?>"><button type="button" class="btn btn-info"><?php the_field('about_us_button_label'); ?></button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc/compliance-footer-section.php'); ?>

    <section id="subscribe" class="padding-top-bottom-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto text-center padding-top-bottom-60 bg-subscribe" aos-init aos-animate data-aos="zoom-in" data-aos-duration="800">
                    <h2 class="heading-bottom-border margin-bottom-30 m-font-size-30">Sign Up for Email Updates</h2>
                    <?php echo do_shortcode('[contact-form-7 id="250" title="Sign Up Email"]'); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>