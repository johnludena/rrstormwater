<?php
/*
 * Template Name: Request a Consultation
 * Template Post Type: reques-a-consultation
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

    <section id="our-insights" class="padding-top-60">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">

                <h2 class="heading-bottom-border"><?php the_title(); ?></h2>
                <p class="lead-paragraph"><?php the_field('text_intro'); ?></p><br/>

            </div>
        </div>
    </div>
</section>

    <section id="contact-form" class="padding-bottom-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center padding-top-bottom-60">
                    <?php echo do_shortcode('[contact-form-7 id="397" title="Request a Consultation"]'); ?>
                </div>
            </div>
        </div>
    </section>


<?php include('inc/compliance-footer-without-section.php'); ?>


<?php get_footer(); ?>