<?php /* Template Name: Corporate Responsibility */ ?>
<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
    <div class="row margin-bottom-100">
        <div class="col-lg-2">
            <?php include('inc/navigation-side.php'); ?>
        </div>
        <div class="col-lg-10">
            <h1><?php echo get_the_title(); ?></h1>
            <p><?php echo get_field("body_content"); ?></p>
        </div>
    </div>
</div>

<?php get_footer(); ?>