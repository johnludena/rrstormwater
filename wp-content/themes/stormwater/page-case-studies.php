<?php
/*
 * Template Name: Case Studies
 * Template Post Type: case-study
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<?php include('inc/hero-landing-banner.php'); ?>

    <?php
                            
    $variable = get_field('compliance_title', 76);
    $variable = get_field('compliance_content', 76);
    $variable = get_field('compliance_button_label', 76);
                            
    ?>

<section id="our-case-studies" class="padding-top-bottom-60 m-padding-top-30">
	<div class="container">
		<div class="row addContList">
            <?php $query_posts = array(
                'post_type' 		=> 'case_study',
                'posts_per_page' 	=> -1,
                'orderby' 			=> 'date',
                'order' 			=> 'ASC'
            ); $insight_posts = new WP_Query( $query_posts ); ?>
            <?php if ( $insight_posts->have_posts() ) : while ( $insight_posts->have_posts() ) : $insight_posts->the_post(); ?>

            <div class="col-lg-4 pb-3">
                <div class="card-image">
                    <img src="https://picsum.photos/680/350" alt="...">
                </div>
                <div class="card-body">
					<h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
					<?php the_advanced_excerpt(); ?>
					<a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                </div>
            </div>
            <?php endwhile; endif; wp_reset_query(); ?>          
        </div>
    </div>
</section>


<?php include('inc/compliance-footer-without-section.php'); ?>


<?php get_footer(); ?>