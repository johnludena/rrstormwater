
<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
	<div class="row">
		<div class="col-lg-2">
			<button class="mobile-sidenav-trigger js-mobile-sidenav-trigger">
				Navigate <span><?php echo get_the_title(); ?></span> <i class="fas fa-chevron-down"></i>
			</button>
			<ul id="sidenav" class="list-unstyled" role="navigation">
				<?php $args = array(
					'post_type' => 'service',
					'post_status' => 'publish',
					'order' => 'ASC',
				 ); $services = new WP_Query( $args ); $parent_page_id = get_queried_object_id(); if( $services->have_posts() ) : ?>
				 
			    <?php while( $services->have_posts() ) : $services->the_post(); ?>
				<?php $postID = get_the_ID(); ?>
					<li class="<?php if($parent_page_id == $postID){echo "current_page_item";}; ?>"><a href="<?php the_permalink(); ?>"><?php printf( '%1$s', get_the_title() );  ?></a></li>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else : esc_html_e( 'No services!', 'text-domain' ); endif; ?>
			</ul>
		</div>
		<div class="col-lg-10">
			<div class="row">
				<div class="col">
					<h1><?php echo get_the_title(); ?></h1>
					<p class="lead-paragraph"><?php echo get_field( "lead_paragraph" ); ?></p>
					<h2><?php echo get_field( "lead_image_title" ); ?></h2>
					<img src="<?php echo get_field('lead_image'); ?>" alt="" class="img-fluid">
					<?php echo get_field( "body_content" ); ?>
				</div>
			</div>
			<div class="row addContList">
				<?php if( have_rows('image_card') ): while ( have_rows('image_card') ) : the_row(); ?>
				<div class="col-lg-4">
					<div class="card-image">
						<img src="<?php the_sub_field('image_card_image'); ?>" alt="">
					</div>
					<div class="card-body">
						<h3><?php the_sub_field('image_card_title'); ?></h3>
						<p><?php the_sub_field('image_card_description'); ?></p>
					</div>
				</div>
				<?php endwhile; endif; ?>
			</div>
			<?php if( get_field('whitepaper_title') ): ?>
			<div class="row text-white bg-blue-dark padding-50 border-radius-5 margin-top-bottom-25">
				<div class="col-lg-12">		
					<h3 class="text-white"><?php echo get_field('whitepaper_title') ?></h3>
					<p><?php echo get_field('whitepaper_description') ?></p>
					<?php echo do_shortcode('[contact-form-7 id="1174" title="White Papers Submission"]'); ?>
					<a class="text-white section-cta caret-right" href="<?php the_permalink(); ?>">Download now</a>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php include('inc/compliance-footer-section.php'); ?>
	
<?php get_footer();