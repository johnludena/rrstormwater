<?php /* Template Name: Detail Sub Page */ ?>

<?php get_header(); ?>

<?php while( have_posts() ) : the_post(); get_template_part( 'template-parts/content', 'page' ); if( comments_open() || get_comments_number() ) {
        comments_template();
} endwhile; ?>

<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
        <div class="row">
                <div class="col-lg-2">
                    <?php include('inc/navigation-side.php'); ?>
                </div>

                <div class="col-lg-10">
                        <div class="row padding-bottom-100">
                                <div class="col">
                                        <h1><?php echo get_the_title(); ?></h1>
                                        <p class="lead-paragraph"><?php echo get_field( "lead_paragraph" ); ?></p>
                                        <h2><?php echo get_field( "lead_image_title" ); ?></h2>
                                        <img src="<?php echo get_field('lead_image'); ?>" alt="" class="img-fluid">
                                        <?php echo get_field( "body_content" ); ?>
                                </div>
                        </div>
                        <div class="row addContList">
                        <?php if( have_rows('image_card') ): while ( have_rows('image_card') ) : the_row(); ?>
                                <div class="col-lg-4">
                                        <div class="card-image">
                                                <img src="<?php the_sub_field('image_card_image'); ?>" alt="">
                                        </div>
                                        <div class="card-body">
                                                <h3><?php the_sub_field('image_card_title'); ?></h3>
                                                <p><?php the_sub_field('image_card_description'); ?></p>
                                        </div>
                                </div>
                        <?php endwhile; endif; ?>
                        </div>                  
                </div>
        </div>
</div>

    <section id="compliance" class="padding-top-60">
        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8 col-md-12 col-sm-12 ">
                        <p class="text-white font-size-36" data-aos="fade-up" data-aos-duration="800"><strong><?php echo get_field('need_help_title'); ?></strong><br>
                        <?php echo get_field('need_help_description'); ?></p>
                    </div>
                    <div class="col-auto mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" target="_blank" href="<?php echo get_field('need_help_button_url'); ?>" aos-init aos-animate data-aos="zoom-in" data-aos-duration="1000"><?php echo get_field('need_help_button_label'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>