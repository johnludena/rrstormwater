<?php get_header(); ?>

<?php include('inc/global/global-page-banner.php'); ?>

<div class="container padding-top-25 padding-bottom-60 m-padding-bottom-30">
	<div class="row">
		<div class="col">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
<?php endwhile; endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-9">
<?php gravity_form(2, false, false, false, '', true); ?>
		</div>
		<div class="col-lg-3">
			<aside>
				<?php the_field('contact_sidebar_content'); ?>
				<ul class="list-unstyled m-0 p-0">
					<?php if( get_field('display_facebook', 'option') ) : ?>
						<li><a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank"><i class="fab fa-facebook"></i></a></li>
					<?php endif; ?>

					<?php if( get_field('display_linkedin', 'option') ) : ?>
						<li><a href="<?php the_field('linkedin_url', 'option'); ?>" target="_blank"><i class="fab fa-linkedin"></i></a></li>
					<?php endif; ?>
				</ul>
			</aside>
		</div>	
	</div>
</div>

<?php include('inc/global/global-prefooter.php'); ?>

<?php get_footer(); ?>