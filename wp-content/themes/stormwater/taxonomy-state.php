<!-- LOCKED CONTENT MODAL -->
<script type="text/javascript">

    function getCookie(name) {
        var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return v ? v[2] : null;
    }

    function setCookie(name, value, days) {
        var d = new Date;
        d.setTime(d.getTime() + 24*60*60*1000*days);
        document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
    }

    var cookie = getCookie('emailSubmission');

    if (cookie === null) {
        document.addEventListener('DOMContentLoaded', function() {
            setTimeout(function(){
                document.querySelector('body').classList.add('locked');
            }, 1000);

            // hide modal on successful CF7 signup
            document.addEventListener('wpcf7mailsent', function (event) {
                document.querySelector('body').classList.remove('locked');
                document.querySelector('#locked-content-modal').style.display = 'none';
                setCookie('emailSubmission', true, 90);
            });
        })

    }

    else {
        document.addEventListener('DOMContentLoaded', function() {

            document.addEventListener('wpcf7mailsent', function (event) {
                document.querySelector('body').classList.remove('locked');
                document.querySelector('#locked-content-modal').style.display = 'none';
            });
        })
    }



</script>

<section id="locked-content-modal" class="text-center">
    <div class="locked-content-bg"></div>
    <div class="form-wrapper">
        <h1 class="font-size-34 margin-bottom-30">Please submit your name and email to access this content</h1>
        <?php echo do_shortcode('[contact-form-7 id="1203" title="Compliance Locked Content"]'); ?>
    </div>
</section>
<!-- END -->

<div class="locked-content-wrapper">
    <?php get_header(); ?>

    <div id="breadcrumbContainer">

        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                        <?php if (function_exists('bcn_display')) {
                            bcn_display();
                        } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <section id="our-insights" class="padding-top-60">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">

                    <h2 class="heading-bottom-border"><?php single_term_title(); ?></h2>
                    <!--                 <p class="lead-paragraph"><?php the_field('text_intro'); ?></p><br/> -->

                </div>
            </div>
        </div>
    </section>

    <section id="contact-form" class="padding-top-bottom-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8 padding-bottom-60">

                    <?php
                    get_posts(array(
                            'post_type' => 'compliance_item',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'state',
                                    'field' => 'term_id',
                                    'terms' => 1)
                            ))
                    );

                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            ?>

                            <br><a href="<?PHP echo get_post_permalink(); ?>"><?PHP echo get_the_title(); ?> <br/></a>

                        <?php

                        endwhile;
                    endif;


                    ?>

                </div>
            </div>
        </div>
    </section>


    <section id="compliance" class="padding-top-60">
        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8">
                        <p class="text-white font-size-36"><strong>Need help with a project?</strong><br>
                            Contact our team to get started today!</p>
                    </div>
                    <div class="col-auto mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" href="#">Request a consultation</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php get_footer(); ?>
</div>

