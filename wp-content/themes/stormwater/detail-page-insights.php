<?php /* Template Name: Insights - Detail Page */ ?>
<?php get_header(); ?>
                                <?php
                                // Start the loop.
                                while ( have_posts() ) : the_post();
                                                // Include the page content template.
                                                get_template_part( 'template-parts/content', 'page' );
                                                // If comments are open or we have at least one comment, load up the comment template.
                                                if ( comments_open() || get_comments_number() ) {
                                                                comments_template();
                                                }
                                                // End of the loop.
                                endwhile;
                                ?>


<div id="breadcrumbContainer">

        <div class="container" >
                <div class="row">
                        <div class="col">
                                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                                    <?php if(function_exists('bcn_display'))
                                    {
                                        bcn_display();
                                    }?>
                                </div>                  
                        </div>
                </div>
        </div>
        
</div>





<div class="container" id="subpageContainer">
        <div class="row">
                <div class="col-2">
                        
                        <ul id="sidenav">



                                <div id="parent-<?php the_ID(); ?>" class="parent-page">

                                    <li><a class="<?php if($parent_page_id == $postID){echo "active";}; ?>" href="#">Blogs</a></li>
                                    <li><a class="<?php if($parent_page_id == $postID){echo "active";}; ?>" href="#">News</a></li>
                                    <li><a class="<?php if($parent_page_id == $postID){echo "active";}; ?>" href="#">Events</a></li>

                                </div>

                        </ul>

                </div>
                <div class="col-10">
                        <div class="row">
                                <div class="col">
                                        <h1><?PHP echo get_the_title(); ?></h1>
                                        <p class="lead-paragraph"><?PHP echo get_field( "lead_paragraph" ); ?></p>
                                        <h2><?PHP echo get_field( "lead_image_title" ); ?></h2>
                                        <img src="<?PHP echo get_field('lead_image'); ?>" alt="">
                                        <p><?PHP echo get_field( "body_content" ); ?></p>
                                </div>
                        </div>
                        <div class="row addContList">
                        <?php

                        // check if the repeater field has rows of data
                        if( have_rows('image_card') ):

                                // loop through the rows of data
                            while ( have_rows('image_card') ) : the_row();
                                ?>


                                <div class="col-4">
                                        <img src="<?php the_sub_field('image_card_image'); ?>" alt="" class="img-fluid">
                                        <h3><?php the_sub_field('image_card_title'); ?></h3>
                                        <p><?php the_sub_field('image_card_description'); ?></p>
                                </div>


                                
                                
                                <?php 
                            endwhile;

                        else :

                            // no rows found

                        endif;

                        ?>

                                


                        </div>                  
                </div>
        </div>

</div>





</div><!-- .content-area -->

<?php get_footer(); ?>