<?php
/*
 * Template Name: Landing Page
 * Template Post Type: landing-page
*/

?>

<?php get_header(); ?>

<?php include('inc/breadcrumbs.php'); ?>

<section id="hero-banner">
    <div class="royalSlider royalSlider--hero rsDefault">
        <div class="rsContent">
            <img class="rsImg" src="<?php the_field('banner_image'); ?>" alt="">

            <div class="slide-text">
                <div class="rsABlock">
                    <div class="inner-text-wrapper">
                        <h1><?php the_field('banner_title'); ?></h1>
                        <p><?php the_field('banner_description'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="our-expertise" class="padding-top-60 m-padding-top-30">
	<div class="container">
		<div class="row addContList">
            <?php $args = array(
                'post_type'      => 'page',
                'posts_per_page' => -1,
                'post_parent'    => $post->ID,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            );  $parent = new WP_Query( $args ); if( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
    
            <div class="col-lg-4 pb-3">
                <div class="card-image">
                    <img src="https://picsum.photos/680/350" alt="">
                </div>
                <div class="card-body">
                    <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <?php echo custom_field_excerpt(); ?>
                    <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                </div>
            </div>
            
            <?php endwhile; endif; wp_reset_query(); ?>          
        </div>
    </div>
</section>

<section id="compliance" class="padding-top-60 m-padding-top-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 justify-content-center">
                <div class="card">
                    <img src="https://picsum.photos/680/350" class="card-img-top" alt="">
                    <div class="card-heading card-body bg-gray-light">
                        <p class="card-category">Regulatory Compliance</p>

                        <h5 class="card-title m-0">
                            <?php $title = get_field('compliance_title', 76); 
                                if(isset($title)) {
                                    print($title);
                            } ?>
                        </h5>
                    </div>
                    <div class="card-body">
                        <?php  $content = get_field('compliance_content', 76); 
                            if(isset($content)) {
                                print($content); 
                            } ?>

                        <a class="section-cta caret-right" href="#">
                        <?php $content = get_field('compliance_button_label', 76); 
                            if(isset($content)) {
                                print($content);    
                            } ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 justify-content-center">
                <div id="featured" class="card h-100">
                    <?php query_posts('posts_per_page=1'); if (have_posts()) while (have_posts()) : the_post(); ?>
                        <img src="https://picsum.photos/680/350" class="card-img-top" alt="">
                        <div class="card-heading card-body bg-gray-light">
                            <p class="card-category">Featured Insight</p>
                            <h5 class="card-title m-0"><?php the_title(); ?></h5>
                        </div>
                        <div class="card-body h-100">
                            <?php the_excerpt(); ?>
                            <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="waves-bg padding-top-60 padding-bottom-90 m-padding-top-30 m-padding-bottom-30">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-8">
                    <p class="text-white font-size-34 m-font-size-22"><strong><?php echo get_field('need_help_title'); ?></strong><br>
                    <?php echo get_field('need_help_description'); ?></p>
                </div>
                <div class="col-lg-auto mx-auto">
                    <a class="btn btn btn-outline-light btn-lg" href="<?php echo get_field('need_help_button_url'); ?>"><?php echo get_field('need_help_button_label'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>