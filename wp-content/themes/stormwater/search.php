<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Shape
 * @since Shape 1.0
 */

get_header(); ?>

<style>

    section.search-content-area {
        min-height: 410px;
    }
        .search-content-area h1 {
            margin-bottom: 40px;
            color: #17a2b8;
            font-size: 50px;
        }

        .single-result-container {
            border-bottom: 1px solid #d7e0e5;
            border-top: 1px solid #d7e0e5;
        }
        
        .single-result-container h2 {
            font-size: 30px;
        }

        .single-result-container a {
            color: #0F2E4E;
            font-weight: bold;
        }
</style>

<div class="container">
    <div class="row">
        <div class="col">

            <section id="primary" class="search-content-area padding-top-bottom-40">
            <div id="content" class="site-content" role="main">

            <?php if ( have_posts() ) : ?>

                <header class="page-header">
                    <h1 class="page-title margin-bottom-40"><?php printf( __( 'Search Results for: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                </header><!-- .page-header -->

                <?php// shape_content_nav( 'nav-above' ); ?>

                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="single-result-container padding-top-bottom-20">
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <p><?php the_excerpt(); ?></p>
                        <p><a class="section-cta caret-right" href="<?php the_permalink(); ?>">Learn more</a></p>
                    </div>

                <?php endwhile; ?>

                <?php// shape_content_nav( 'nav-below' ); ?>

            <?php else : ?>
                <h1>No results found</h1>
                <p>Please try another search term above.</p>
                <?php// get_template_part( 'no-results', 'search' ); ?>

            <?php endif; ?>

            </div><!-- #content .site-content -->
        </section><!-- #primary .content-area -->
        </div>
    </div>
</div>

<section id="compliance" class="padding-top-60">
    <div class="waves-bg padding-top-60 padding-bottom-90">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-8">
                    <p class="text-white font-size-36"><strong>Need help with a project?</strong><br>
                    Contact our team to get started today!</p>
                </div>
                <div class="col-auto mx-auto">
                    <a class="btn btn btn-outline-light btn-lg" href="#">Request a consultation</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>