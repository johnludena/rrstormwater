
<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
    <div class="row margin-bottom-100">
        <div class="col">
            <h1><?php echo get_the_title(); ?></h1>
            <p><?php echo get_post_field('post_content', $post->ID); ?></p>
<!--             <h3><?php echo get_post_field('client_name', $post->ID); ?></h3>
            <h5>
            <?php
            $location = get_post_field('location', 120); 
                if(isset($location)){
                    print($location); 
                }
            
            $market = get_post_field('market_type', 120); 
                if(isset($market) && !empty($market)){
                    print(' | '.get_the_category_by_ID($market));   
                }

            $service = get_post_field('service_type', 120); 
                if($service->post_title){
                    print(' | '.$service->post_title); 
                } ?>
            </h5>
            <br /> -->
            
            <h3><?php echo get_post_field('lead_title', $post->ID); ?></h3>
            <p><?php echo get_post_field('lead_paragraph', $post->ID); ?></p>
            <h3><?php echo get_post_field('slider_title', $post->ID); ?></h3>
            <p><?php echo get_post_field('slider_paragraph', $post->ID); ?></p>
            <p><?php echo get_post_field('slider', $post->ID); ?></p>
            <p><?php echo get_post_field('before', $post->ID); ?></p>
            <p><?php echo get_post_field('after', $post->ID); ?></p>
            <p><?php echo get_post_field('results_paragraph', $post->ID); ?></p>
        </div>
	</div>
</div>

<?php include('inc/compliance-footer-without-section.php'); ?>


<?php get_footer();