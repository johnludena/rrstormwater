<?php /* Template Name: Executive Team */ ?>
<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

    <div class="container" id="subpageContainer">
        <div class="row margin-bottom-100">
            <div class="col-lg-2">
                <?php include('inc/navigation-side.php'); ?>
            </div>

            <div class="col-lg-10">
                <h1><?php echo get_the_title(); ?></h1>
                <p class="lead-paragraph"><?PHP echo get_field("lead_paragraph"); ?></p>
                <p><?php echo get_field("body_content"); ?></p>

                <div class="row addContList">
<!--                    <style>-->
<!---->
<!--                        /* The flip card container - set the width and height to whatever you want. We have added the border property to demonstrate that the flip itself goes out of the box on hover (remove perspective if you don't want the 3D effect */-->
<!--                        .flip-card {-->
<!--                            background-color: transparent;-->
<!--                            width: 312px;-->
<!--                            height: 450px;-->
<!--                            perspective: 1000px; /* Remove this if you don't want the 3D effect */-->
<!--                            margin-bottom: 30px;-->
<!--                        }-->
<!---->
<!--                        /* This container is needed to position the front and back side */-->
<!--                        .flip-card-inner {-->
<!--                            position: relative;-->
<!--                            width: 100%;-->
<!--                            height: 100%;-->
<!--                            text-align: center;-->
<!--                            transition: transform 0.8s;-->
<!--                            transform-style: preserve-3d;-->
<!--                        }-->
<!---->
<!--                        /* Do an horizontal flip when you move the mouse over the flip box container */-->
<!--                        .flip-card:hover .flip-card-inner {-->
<!--                            transform: rotateY(180deg);-->
<!--                        }-->
<!---->
<!--                        /* Position the front and back side */-->
<!--                        .flip-card-front, .flip-card-back {-->
<!--                            position: absolute;-->
<!--                            width: 100%;-->
<!--                            height: 100%;-->
<!--                            backface-visibility: hidden;-->
<!--                        }-->
<!---->
<!--                        /* Style the front side (fallback if image is missing) */-->
<!--                        .flip-card-front {-->
<!--                            background-color: white;-->
<!--                            color: black;-->
<!--                        }-->
<!---->
<!--                        .flip-card-front h3 {-->
<!--                            margin-top: 10px;-->
<!---->
<!--                        }-->
<!---->
<!---->
<!--                        /* Style the back side */-->
<!--                        .flip-card-back {-->
<!--                            background-color: white;-->
<!--                            color: black;-->
<!--                            border: solid 1px #ccc;-->
<!--                            padding: 30px;-->
<!--                            transform: rotateY(180deg);-->
<!--                        }-->
<!---->
<!--                        .flip-card-back p {-->
<!--                            margin-bottom: 15px;-->
<!--                        }-->
<!---->
<!--                    </style>-->


                    <?php if (have_rows('team')): while (have_rows('team')) : the_row(); ?>
                        <div class="col-lg-4">


                            <div class="flip-card js-eqheight-card-body">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <img src="<?php the_sub_field('team_member_photo'); ?>" alt=""
                                             class="img-fluid">
                                        <h3><?php the_sub_field('team_member_name'); ?></h3>
                                    </div>
                                    <div class="flip-card-back">

                                        <h3 class="text-blue-dark margin-bottom-5"><?php the_sub_field('team_member_name'); ?></h3>
                                        <p class="font-size-14"><?php the_sub_field('team_member_position'); ?></p>
                                        <p><?php the_sub_field('member_bio'); ?></p>
                                        <p><a href="<?php the_sub_field('email'); ?>"><i class="fas fa-envelope"></i></a>
                                            <a href="<?php the_sub_field('linkedin'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!--

                             -->                    <!-- <?php the_sub_field('team_member_photo'); ?> -->


                        </div>
                    <?php endwhile; endif; ?>


                </div>
            </div>
        </div>
    </div>

    <section id="compliance" class="padding-top-60">
        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-9 col-md-12 col-sm-12 ">
                        <p class="text-white font-size-36" data-aos="fade-up" data-aos-duration="800">

                            <?php echo get_field('need_help_description'); ?></p>
                    </div>
                    <div class="col-auto m-mx-auto">
                        <a class="btn btn btn-outline-light btn-lg"
                           href="<?php echo get_field('need_help_button_url'); ?>" aos-init aos-animate
                           data-aos="zoom-in"
                           data-aos-duration="1000"><?php echo get_field('need_help_button_label'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>