<?php
/*
 * Template Name: Single Column
*/


?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>


<section id="main-content" class="padding-top-bottom-60">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">

                <div class="text-center">
                    <h2 class="heading-bottom-border text-align-center"><?php the_title(); ?></h2>
                </div>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                endwhile; else: ?>
                    <p>Sorry, no posts matched your criteria.</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>


<?php include('inc/compliance-footer-without-section.php'); ?>

<?php get_footer(); ?>