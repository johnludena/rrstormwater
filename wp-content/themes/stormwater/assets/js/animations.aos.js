// Functions
// =============================================
function aosInit() {

	// add animations
	$(".aos-fade").wrap('<div class="w-100" data-aos="fade"></div>');
	$(".aos-fade-up").wrap('<div class="w-100" data-aos="fade-up"></div>');
	$(".aos-fade-down").wrap('<div class="w-100" data-aos="fade-down"></div>');
	$(".aos-fade-left").wrap('<div class="w-100" data-aos="fade-left"></div>');
	$(".aos-fade-right").wrap('<div class="w-100" data-aos="fade-right"></div>');

	$(".aos-slide-up").wrap('<div class="w-100" data-aos="slide-up"></div>');
	$(".aos-slide-down").wrap('<div class="w-100" data-aos="slide-down"></div>');
	$(".aos-slide-left").wrap('<div class="w-100" data-aos="slide-left"></div>');
	$(".aos-slide-right").wrap('<div class="w-100" data-aos="slide-right"></div>');

	$(".aos-zoom-in").wrap('<div class="w-100" data-aos="zoom-in"></div>');
	$(".aos-zoom-in-up").wrap('<div class="w-100" data-aos="zoom-in-up"></div>');
	$(".aos-zoom-in-down").wrap('<div class="w-100" data-aos="zoom-in-down"></div>');
	$(".aos-zoom-in-left").wrap('<div class="w-100" data-aos="zoom-in-left"></div>');
	$(".aos-zoom-in-right").wrap('<div class="w-100" data-aos="zoom-in-right"></div>');

	$(".aos-zoom-out").wrap('<div class="w-100" data-aos="zoom-out"></div>');
	$(".aos-zoom-out-up").wrap('<div class="w-100" data-aos="zoom-out-up"></div>');
	$(".aos-zoom-out-down").wrap('<div class="w-100" data-aos="zoom-out-down"></div>');
	$(".aos-zoom-out-left").wrap('<div class="w-100" data-aos="zoom-out-left"></div>');
	$(".aos-zoom-out-right").wrap('<div class="w-100" data-aos="zoom-out-right"></div>');

	// delay modifiers
	$(".aos-delay-300").parent().attr('data-aos-delay', '300');
	$(".aos-delay-400").parent().attr('data-aos-delay', '400');
	$(".aos-delay-500").parent().attr('data-aos-delay', '500');
	$(".aos-delay-600").parent().attr('data-aos-delay', '600');
	$(".aos-delay-700").parent().attr('data-aos-delay', '700');
	$(".aos-delay-800").parent().attr('data-aos-delay', '800');
	$(".aos-delay-900").parent().attr('data-aos-delay', '900');
	$(".aos-delay-1000").parent().attr('data-aos-delay', '1000');
	$(".aos-delay-1100").parent().attr('data-aos-delay', '1100');
	$(".aos-delay-1200").parent().attr('data-aos-delay', '1200');
	$(".aos-delay-1300").parent().attr('data-aos-delay', '1300');
	$(".aos-delay-1400").parent().attr('data-aos-delay', '1400');
	$(".aos-delay-1500").parent().attr('data-aos-delay', '1500');
	$(".aos-delay-1600").parent().attr('data-aos-delay', '1600');
	$(".aos-delay-1700").parent().attr('data-aos-delay', '1700');
	$(".aos-delay-1800").parent().attr('data-aos-delay', '1800');
	$(".aos-delay-1900").parent().attr('data-aos-delay', '1900');
	$(".aos-delay-2000").parent().attr('data-aos-delay', '2000');

	// easing modifiers
	$(".aos-easing-easeinout").parent().attr('data-aos-easing', 'ease-in-out');
	$(".aos-easing-easeinback").parent().attr('data-aos-easing', 'ease-in-back');
	$(".aos-easing-easeoutback").parent().attr('data-aos-easing', 'ease-out-back');
}

// Events
// =============================================
$(window).on('load', function() {
	AOS.init({
		delay: 200,
		easing: 'ease-out-cubic',
		once: true,
		disable: 'mobile'
	});
	aosInit();
});