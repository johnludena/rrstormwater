//nav
$('.nav-thin-client-trigger').on('mouseenter', function (e) {
    $('.nav-thin-client-container').fadeIn("fast");
});

$('.nav-thin-client-container').on('mouseenter', function () {
    $('.nav-thin-client-trigger').addClass("main-nav-hover");
});

$('.nav-resource-trigger, .empty-nav').on('mouseenter', function () {
    $('.nav-thin-client-container').fadeOut("fast");
    $('.nav-thin-client-trigger').removeClass("main-nav-hover");
});



$('.navbar-fixed-top').on('mouseleave', function () {
    $('.nav-thin-client-container').fadeOut("fast");
    $('.nav-thin-client-trigger').removeClass("main-nav-hover");
});

// $('.nav-thin-client-container').on('mouseenter', function(){
//     $('.nav-thin-client-trigger').addClass("main-nav-hover");
// });

// $('.nav-thin-client-container').on('mouseleave', function(){
//     $('.nav-thin-client-container').fadeOut();
//     $('.nav-thin-client-trigger').removeClass("main-nav-hover");
// });

// $('.nav-resource-trigger').on('mouseenter', function(){
//     $('.nav-thin-client-container').fadeOut();
//     $('.nav-thin-client-trigger').removeClass("main-nav-hover");
// });

// $('#navbar').on('mouseleave', function(){
//     $('.nav-thin-client-container').fadeOut();
//     $('.nav-thin-client-trigger').removeClass("main-nav-hover");
// });



$(document).ready(function () {

    "use strict";

    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');

    $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');

    $(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\"></a>");


    $(".menu > ul > li").hover(function (e) {
        if ($(window).width() > 943) {
            $(this).children("ul").stop(true, false).fadeToggle(150);
            e.preventDefault();
        }
    });

    $(".menu > ul > li").click(function () {
        if ($(window).width() <= 943) {
            $(this).children("ul").fadeToggle(150);
        }
    });

    $(".menu-mobile").click(function (e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });

});

