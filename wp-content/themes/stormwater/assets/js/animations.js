(function($) {

    function header() {
        setTimeout(function() {
            $("header a.logo").addClass('fade-in');
        }, 300);

        setTimeout(function() {
            $("header nav").addClass('fade-in');
        }, 400);

        setTimeout(function() {
            $("header a.btn").addClass('fade-in');
        }, 500);
    }

    function homepageBannerBookingBox() {
        setTimeout(function() {
            $(".banner-booking-box").addClass('fade-in-right');
        }, 600);
    }

    function roomsBannerTop() {
        setTimeout(function() {
            $("[id=banner-rooms] h1").addClass('fade-in-down');
        }, 400);

        setTimeout(function() {
            $("[id=banner-rooms] p").addClass('fade-in-up');
        }, 500);
    }

    function diningBannerTop() {
        setTimeout(function() {
            $("[id=banner-dining] h1").addClass('fade-in-down');
        }, 400);
    }

    function specialsBannerTop() {
        setTimeout(function() {
            $("[id=banner-specials] h1").addClass('fade-in-down');
        }, 500);

        setTimeout(function() {
            $("[id=banner-specials] p").addClass('fade-in-up');
        }, 600);
    }
    
    function eventsBannerTop() {
        setTimeout(function() {
            $("[id=banner-events] h1").addClass('fade-in-down');
        }, 500);
    }
    
    function aboutBannerTop() {
        setTimeout(function() {
            $("[id=banner-about] h1").addClass('fade-in-down');
        }, 500);

        setTimeout(function() {
            $("[id=banner-about] p").addClass('fade-in-up');
        }, 600);

        setTimeout(function() {
            $("[id=body-about] .img-special").addClass('fade-in-up');
        }, 600);
    }

    function faqBannerTop() {
        setTimeout(function() {
            $("[id=banner-faq] h1").addClass('fade-in-down');
        }, 500);
    }

    header();
    homepageBannerBookingBox();
    roomsBannerTop();
    diningBannerTop();
    specialsBannerTop();
    eventsBannerTop();
    aboutBannerTop();
    faqBannerTop();

})(jQuery);