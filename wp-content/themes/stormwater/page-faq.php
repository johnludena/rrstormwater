<?php
/*
 * Template Name: FAQ
 * Template Post Type: faq
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

    <section id="our-insights" class="padding-top-60">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h2 class="heading-bottom-border text-center"><?php the_title(); ?></h2>

                <?php
                $faq_questions = get_field('faq_questions', get_the_ID());
                $count = count($faq_questions);

                if (!$faq_questions) {
                    return;
                }
                ?>


                <?php foreach ($faq_questions as $item):

                    $question = $item['question'];
                    $answer = $item['answer'];

                ?>

                <h3><?php echo $question; ?></h3><br/>
                <p class="lead-paragraph"><?php echo $answer; ?></p><br/>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
</section>

    <section id="contact-form" class="padding-top-bottom-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8 padding-bottom-60">
                    <?php echo get_post_field('post_content', $post->ID); ?>
                </div>
            </div>
        </div>
    </section>


<?php include('inc/compliance-footer-without-section.php'); ?>

<?php get_footer(); ?>