<?php /* Template Name: About Us - Detail Page */ ?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
        <div class="row">
                <div class="col-lg-2">
                    <?php include('inc/navigation-side.php'); ?>
                </div>

                <div class="col-lg-10">
                        <div class="row">
                                <div class="col-lg-12">
                                        <h1><?php echo get_the_title(); ?></h1>
                                        <p class="lead-paragraph"><?php echo get_field( "lead_paragraph" ); ?></p>
                                        <h2><?php echo get_field( "first_title" ); ?></h2>
                                        <p><?php echo get_field( "first_title_desc" ); ?></p>
                                </div>
                        </div>


                    <?php
                    $career_card = get_field('card_repeater', get_the_ID());
                    $count = count($career_card);

                    if (!$career_card) {
                        return;
                    }
                    ?>

                    <div class="row">
                        <?php foreach ($career_card as $item):

                            if ($count == 1) {
                                $class = 'col-lg-12';
                            } elseif ($count == 2) {
                                $class = 'col-lg-6';
                            } else {
                                $class = 'col-lg-4';
                            }

                            $icon = $item['card_icon'];
                            $title = $item['card_title'];
                            $card_desc = $item['card_desc'];

                            ?>
                            <div class="<?php echo $class; ?> text-center card-body d-flex align-items-stretch">
                                <div class="padding-50 bg-gray-light">
                                    <span class="careers-icon" data-aos="fade-up" data-aos-duration="600"><?php echo $icon; ?></span>
                                    <h3 class="padding-top-bottom-25" data-aos="fade-up" data-aos-duration="800"><?php echo $title ?></h3>
                                    <p data-aos="fade-up" data-aos-duration="1000"><?php echo $card_desc; ?></p>
                                </div>
                            </div>

                        <?php endforeach; ?>



                    </div>



                        <div class="row padding-top-50">
                                <div class="col-lg-12">
                                        <h2><?php echo get_field( "second_title" ); ?></h2>
                                        <p><?php echo get_field( "second_title_desc" ); ?></p>
                                </div>
                        </div>


                    <?php
                    $career_box = get_field('second_card_repeater', get_the_ID());
                    $count = count($career_box);

                    if (!$career_box) {
                        return;
                    }
                    ?>

                    <div class="row padding-bottom-50">
                        <?php foreach ($career_box as $item):

                            if ($count == 1) {
                                $class = 'col-lg-12';
                            } elseif ($count == 2) {
                                $class = 'col-lg-6';
                            } else {
                                $class = 'col-lg-6';
                            }

                            $title = $item['card_title'];
                            $card_desc = $item['card_desc'];

                            ?>
                            <div class="<?php echo $class; ?> text-center card-body d-flex align-items-stretch" data-aos="flip-up" data-aos-duration="400">
                                <div class="padding-25 about-border">
                                    <h3><?php echo $title ?></h3>
                                    <p><?php echo $card_desc; ?></p>
                                </div>
                            </div>

                        <?php endforeach; ?>



                    </div>


                        <div class="row addContList">
                        <?php

                        // check if the repeater field has rows of data
                        if( have_rows('image_card') ):

                                // loop through the rows of data
                            while ( have_rows('image_card') ) : the_row();
                                ?>


                                <div class="col-lg-4">
                                        <img src="<?php the_sub_field('image_card_image'); ?>" alt="" class="img-fluid">
                                        <h3><?php the_sub_field('image_card_title'); ?></h3>
                                        <p><?php the_sub_field('image_card_description'); ?></p>
                                </div>
                                
                                
                                <?php 
                            endwhile;

                        else :

                            // no rows found

                        endif;

                        ?>

                                


                        </div>                  
                </div>
        </div>

</div>





</div><!-- .content-area -->

    <section id="compliance" class="padding-top-60">
        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8 col-md-12 col-sm-12 ">
                        <p class="text-white font-size-36" data-aos="fade-up" data-aos-duration="800"><strong><?php echo get_field('need_help_title'); ?></strong><br>
                        <?php echo get_field('need_help_description'); ?></p>
                    </div>
                    <div class="col-auto mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" target="_blank" href="<?php echo get_field('need_help_button_url'); ?>" aos-init aos-animate data-aos="zoom-in" data-aos-duration="1000"><?php echo get_field('need_help_button_label'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>