<?php
/*
 * Template Name: Contact Us
 * Template Post Type: contact-us
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>
<?php include('inc/hero-landing-banner.php'); ?>
    <section id="contact-form" class="padding-top-60">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8 col-sm-6 padding-top-bottom-60 padding-right-60 contact-us-form">
                    <?php echo do_shortcode('[contact-form-7 id="599" title="Get In Touch"]'); ?>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-6 padding-top-bottom-60 padding-left-60">
                     <?php the_field('body_content'); ?>
                </div>
            </div>
        </div>
    </section>

<?php include('inc/compliance-footer-section.php'); ?>

<?php get_footer(); ?>