<?php /* Template Name: Expertise - Detail Page */ ?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
    <div class="row padding-bottom-100 m-padding-bottom-0">
            <div class="col-lg-2">
                <?php include('inc/navigation-side.php'); ?>
            </div>

            <div class="col-lg-10">
                <div class="row">
                    <div class="col">

                        <h1><?php echo get_the_title(); ?></h1>
                        <p class="lead-paragraph"><?php echo get_field( "lead_paragraph" ); ?></p>
                        <h2><?php echo get_field( "lead_image_title" ); ?></h2>

                        <?php if( get_field('lead_image') ) : ?>
                        <img src="<?php echo get_field('lead_image'); ?>" alt="">
                        <?php endif; ?>

                        <div class="row padding-bottom-30">
                        <?php if( have_rows('client_logos') ): while( have_rows('client_logos') ): the_row();
                                        // display a sub field value
                                        $logo = get_sub_field('logo');
                                    ?>
                        
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <img class="img-fluid" src="<?php echo $logo; ?>" alt="">
                            </div>
                        
                        <?php endwhile; endif; ?>
                        </div>


                        <?php echo get_field( "body_content" ); ?>
                    </div>
                </div>
                <div class="row addContList">
                    <?php if( have_rows('image_card') ): while ( have_rows('image_card') ) : the_row(); ?>
                    <div class="col-lg-4">
                        <div class="card-image">
                            <img src="<?php the_sub_field('image_card_image'); ?>" alt="">
                        </div>
                        <div class="card-body">
                            <h3><?php the_sub_field('image_card_title'); ?></h3>
                            <p><?php the_sub_field('image_card_description'); ?></p>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                </div>                  
            </div>
        </div>
    </div>

<?php include('inc/compliance-footer-without-section.php'); ?>

</div>

<?php get_footer(); ?>