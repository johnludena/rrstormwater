<?php
/*
 * Template Name: Services Page
 * Template Post Type: service
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<?php include('inc/hero-landing-banner.php'); ?>

<section id="our-expertise" class="padding-top-60 m-padding-top-30">
	<div class="container">
		<div class="row addContList">
	        <?php $args = array(
                'post_type'      => 'service',
                'posts_per_page' => -1,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            ); $parent = new WP_Query( $args ); if ( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            
            <div class="col-lg-4 pb-3">
                <div class="card-image">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="card-body ">
                    <div class="js-eq-height-happenings-store">
                        <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="card-text">
                            <?php echo custom_field_excerpt(); ?>
                        </div>
                    </div>
                    <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                </div>
            </div>
            
            <?php endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
</section>


<?php include('inc/compliance-footer-section.php'); ?>

<?php get_footer(); ?>