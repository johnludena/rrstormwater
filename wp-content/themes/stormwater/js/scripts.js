// Debounce
// ----------------------------------------------------------
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this,
			args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

// Start jquery stuffs
// ----------------------------------------------------------
(function ($) {

	// Turn off Microsoft Edge smooth mouse scrolling bcuz it's not so smooth
	// ----------------------------------------------------------
	if(navigator.userAgent.match(/MSIE 10/i) || navigator.userAgent.match(/Trident\/7\./) || navigator.userAgent.match(/Edge\/12\./)) {
		$('body').on("mousewheel", function () {
			event.preventDefault();
			var wd = event.wheelDelta;
			var csp = window.pageYOffset;
			window.scrollTo(0, csp - wd);
		});
	}

	// Initialize forms
    // =============================================
    function initForms() {

        // Clear placeholder copy
        var inputs = $("input, textarea");
        var i = 0;
        while (i < inputs.length) {
            $(inputs[i]).attr('data-placeholder', $(inputs[i]).attr('placeholder'));
            i += 1;
        }

        inputs.on('focus', function () {
            $(this).attr('placeholder', '');
        });

        inputs.on('blur', function () {
            $(this).attr('placeholder', $(this).attr('data-placeholder'));
        });
    }

    initForms();

    $(document).on('ready', function() {
        initForms();
    });

    $(document).ajaxComplete(function(event, xhr, settings) {
        initForms();
    });

	// Sticky nav adds class when scrolled
	// ----------------------------------------------------------
	$(window).scroll(function() {
		var scrolled = $(window).scrollTop();
	
		if( scrolled > 1 ) {
			$(".fixed-top-nav-container").addClass('scrolled');
		} else {
			$(".fixed-top-nav-container").removeClass('scrolled');
		}
	});





	// Main navigation
	// ----------------------------------------------------------
	function addArrow() {
		$("ul[id=menu-primary-nav] li.menu-item-has-children").append('<span><i class="fas fa-chevron-down"></i></span>');
	}
	addArrow();

	// Debounce
    var draw = debounce(function() {
		var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

		if( windowWidth <= 991 ) {
			$("ul.sf-menu").superfish('destroy');
			$(".menu-primary-nav-container").find("ul[id=menu-primary-nav]").removeClass('sf-menu');
			// Add attributes to the li who have children, add attributes to their sub menu containers
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").each(function() {
				$(this).attr('aria-expanded', 'false').attr('aria-controls', 'sub-' + $(this).parent().attr('id'));
				$(this).parent().find("ul.sub-menu").attr('aria-hidden', 'true').attr('id', 'sub-' + $(this).parent().attr('id'));
			});
			
		} else {
			$(".menu-primary-nav-container").find("ul[id=menu-primary-nav]").addClass('sf-menu');
			$("ul.sf-menu").superfish({
				cssArrows: false,
				delay: 300,
				speed: 'fast',
                speedOut: 0,
                disableHI: true,
			});
			// Remove attributes to the li who have children, add attributes to their sub menu containers
			$("ul[id=menu-primary-nav] li.menu-item-has-children span").removeAttr('aria-expanded', 'false').removeAttr('aria-controls', 'sub-' + $(this).parent().attr('id'));
			$("ul[id=menu-primary-nav] li.menu-item-has-children span").parent().find("ul.sub-menu").removeAttr('aria-hidden', 'true').removeAttr('id', 'sub-' + $(this).parent().attr('id'));
		}

	}, 100);
	draw();
	window.addEventListener('resize', draw);
	
	function mobileSubmenuToggle() {

		if( $(this).hasClass('open') ) {
			// Close the sub menu
			$(this).removeClass('open');
			$(this).parent().find("ul.sub-menu").stop().slideUp('fast');
			$(this).find(".fas").removeClass('fa-chevron-up');
			$(this).find(".fas").addClass('fa-chevron-down');
			// Set attributes
			$(this).attr('aria-expanded', 'false');
            $(this).parent().find("ul.sub-menu").attr('aria-hidden', 'true');
		} else {
			// Reset the whole gang
			$("ul[id=menu-primary-nav] li.menu-item-has-children span").removeClass('open');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").parent().find("ul.sub-menu").slideUp('fast');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").find(".fas").removeClass('fa-chevron-up');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").find(".fas").addClass('fa-chevron-down');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").attr('aria-expanded', 'false');
			$("ul[id=menu-primary-nav] li.menu-item-has-children span").parent().find("ul.sub-menu").attr('aria-hidden', 'true');
			// Open the sub menu
			$(this).addClass('open');
			$(this).parent().find("ul.sub-menu").stop().slideDown('fast');
			$(this).find(".fas").removeClass('fa-chevron-down');
			$(this).find(".fas").addClass('fa-chevron-up');
			// Set attributes
			$(this).attr('aria-expanded', 'true');
            $(this).parent().find("ul.sub-menu").attr('aria-hidden', 'false');
		}
	}
	$(document).on('click', 'ul[id=menu-primary-nav] li.menu-item-has-children span', mobileSubmenuToggle);
	
	// Mobile menu
	// ----------------------------------------------------------
	var mobileTrigger = $("a.mobile-nav-trigger");

	function openMobileTarget(e) {
		e.preventDefault();

        if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$(this).attr('title', 'Open menu');
			$(".nav--navbar-primary-wrapper").stop().slideUp('fast');

			$("ul[id=menu-primary-nav] li.menu-item-has-children span").removeClass('open');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").parent().find("ul.sub-menu").slideUp('fast');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").find(".fas").removeClass('fa-chevron-up');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").find(".fas").addClass('fa-chevron-down');
            $("ul[id=menu-primary-nav] li.menu-item-has-children span").attr('aria-expanded', 'false');
			$("ul[id=menu-primary-nav] li.menu-item-has-children span").parent().find("ul.sub-menu").attr('aria-hidden', 'true');
        } else {
			$(this).addClass('open');
			$(this).attr('title', 'Close menu');
			$(".nav--navbar-primary-wrapper").stop().slideDown('fast');
        }
	}
	mobileTrigger.on('click', openMobileTarget);


	$(".js-stats-num-a, .js-stats-num-b").counterUp({
    	delay: 10,
	    time: 1000
	});


	// Royalslider homepage hero
	// -----------------------------------------------------------------------
	var homepageSlider = $("div.royalSlider--hero");

	// If the slider exists
	if (homepageSlider.length) {

		// Display slides after the banner exists, prevents flicker on page load
		homepageSlider.find(".rsContent").css('display', 'block');

		// Slider options
		var homepageSliderOptions = {
			keyboardNavEnabled: true,
			arrowsNavAutoHide: false,
			controlNavigation: 'none',
			imageScaleMode: 'fill',
			slidesSpacing: 0,
			transitionSpeed: 400,
			usePreloader: true,
			loop: true,
		}

		// Initiate slider with options
		homepageSlider.royalSlider(homepageSliderOptions);

		// Get the slider instance
		var homepageSliderHero = $("div.royalSlider--hero:not(.royalSlider--services)"),
			homepageSliderData = homepageSlider.data('royalSlider');

		// If only 1 slide
		if (homepageSliderData.numSlides == 1) {
		  $("div.rsNav").hide();
		  // Disable dragging
		  homepageSliderHero.find("div.rsOverflow").addClass('rsNoDrag');
		}
	}



	// EXECUTIVE LEADERSHIP CARDS
	// =============================================
	$('.flip-card').on('click', function(e) {
		$(this).find('.flip-card-front').toggle();
		$(this).find('.flip-card-back').toggle();

	})

	// SEARCH
	// =============================================

	// Expandable search
	function toggleSearch() {
		$('form.expandable-search').toggleClass('open');
		$('#menu-utility-nav').toggleClass('search-active');

		$('form.expandable-search input[type="text"]').focus();
	}

	$('form.expandable-search button, form.expandable-search .fa-times').on('click', function(e){
		e.preventDefault();
		toggleSearch();
	});

	// close search when clicking outside search area without event.stopPropagation();
	$(document).on('click', function(e) {

		var expandableSearch = $('form.expandable-search');
		var menuNav = $('#menu-utility-nav');

		if (!$(e.target).closest(expandableSearch).length) {

			expandableSearch.removeClass('open');
			menuNav.removeClass('search-active');

			return;
		}

	});

	// Submit search query
	$('form.expandable-search input[type="text"]').on('keydown', function (e) {
		console.log('key event')

		if (e.key === 'Enter') {

			// check for beta.adcetera and staging.adcetera.com or local dev environments
			if (window.location.host.includes('adcetera.com') || window.location.host.includes('localhost') ) {
				var baseUrl = window.location.origin + '/rrstormwater';
				var inputValue = $(this).val();

				window.location = baseUrl + '?s=' + inputValue;
			}

			// set Production paths on client's server
			else {
				var baseUrl = window.location.origin;
				var inputValue = $(this).val();

				window.location = baseUrl + '?s=' + inputValue;
			}
		}

	});

	// TWENTY/TWENTY (before/after image plugin)
	// =============================================
	$(window).on('load', function () {
		$(".twentytwenty-container").twentytwenty();
	});
	
	// HOMEPAGE - tab system (set initial state)
	// TODO: see if this can be updated on PHP to set class on first elements
	$('#services-tabs .nav-pills .nav-link:first-child').addClass('active');
	$('#services-tabs .tab-content .tab-pane:first-child').addClass('show active');

	// Mobile navigation menu button
	// ----------------------------------------------------------
	function mobileNav() {

		if( $(this).hasClass('open') ) {
			$(this).removeClass('open');
			$(this).find("i").addClass('fa-chevron-down');
			$(this).find("i").removeClass('fa-chevron-up');
			$(this).next().slideUp('fast');
		} else {
			$(this).addClass('open');
			$(this).find("i").addClass('fa-chevron-up');
			$(this).find("i").removeClass('fa-chevron-down');
			$(this).next().slideDown('fast');
		}
	}
	$(document).on('click', '.js-mobile-sidenav-trigger', mobileNav);

	// Back to top fade
	// =============================================
	$(window).scroll(function() {
		var scrolled = $(window).scrollTop();

		if( scrolled > 400 ) {
			$(".back-to-top").addClass('visible');
		} else {
			$(".back-to-top").removeClass('visible');
		}
	});


	$(".back-to-top").on('click', function(e) {
		e.preventDefault();
	$("html, body").animate({
	        scrollTop: $("[id=top]").offset().top
	    }, 300);
	});

	// Equal height cards
	// =============================================
	window.initHeights = function () {
		var draw = debounce(function () {

			$(".js-eqheight-card-body").samesizr();

		}, 250);
		draw();
	}

	window.initHeights();
	window.addEventListener('resize', window.initHeights);

	// Fill cards either horizontally or vertically to prevent gaps
	var fillCardBackground = debounce(function () {
		var cardImgArr = $('.card--insights .card-img-container img, section#hero-banner .royalSlider--hero .rsContent img'); // select images that are bg's only

		cardImgArr.each(function(){

			var cardContainer = $(this).closest('div');

			var cardImgAspectRatio = $(this).width() / $(this).height();
			var cardContainerAspectRatio = cardContainer.width() / cardContainer.height();

			if (cardContainerAspectRatio < cardImgAspectRatio) {
				$(this).removeClass('wide').addClass('tall');
			} else {
				$(this).removeClass('tall').addClass('wide');
			}
		})


	}, 250)

	$(window).on('load', fillCardBackground);
	window.addEventListener("resize", fillCardBackground); // put on debounce for responsive


	// Style alert overrides for Contact Form 7 
	document.addEventListener('wpcf7invalid', function (event) {
		$('.wpcf7-response-output').addClass('alert alert-danger margin-top-bottom-20');
	}, false);
	document.addEventListener('wpcf7spam', function (event) {
		$('.wpcf7-response-output').addClass('alert alert-warning margin-top-bottom-20');
	}, false);
	document.addEventListener('wpcf7mailfailed', function (event) {
		$('.wpcf7-response-output').addClass('alert alert-warning margin-top-bottom-20');
	}, false);
	document.addEventListener('wpcf7mailsent', function (event) {
		$('.wpcf7-response-output').addClass('alert alert-success margin-top-bottom-20');
	}, false);

})(jQuery);

// AOS
// =============================================
AOS.init({
	delay: 200,
	easing: 'ease-out-expo',
	once: true,
	disable: 'mobile'
});

window.addEventListener('load', AOS.refresh); // Yes we need this