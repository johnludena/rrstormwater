<?php
/*
 * Template Name: Compliance
 * Template Post Type: compliance
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<?php include('inc/hero-landing-banner.php'); ?>

<?php

$variable = get_field('compliance_title', 76);
$variable = get_field('compliance_content', 76);
$variable = get_field('compliance_button_label', 76);

?>

    <section id="our-compliances" class="padding-top-bottom-60">
        <div class="container">
            <div class="row addContList">
                <?php

                $args = array(
                    'post_type' => 'page',
                    'posts_per_page' => -1,
                    'post_parent' => 358,
                    'order' => 'ASC',
                    'orderby' => 'menu_order'
                );


                $parent = new WP_Query($args);

                if ($parent->have_posts()) : ?>

                    <?php while ($parent->have_posts()) : $parent->the_post(); ?>
                        <div class="col-lg-6 col-md-6 pb-3">
                            <div class="card card--insights card--large">
                                <?php
                                $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');

                                if( !empty($featured_img_url) ): ?>
                                    <div class="card-img-container">
                                        <a href="<?php the_permalink(); ?>">
                                            <img src="<?php echo $featured_img_url; ?>" class="card-img-top" alt="" />
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="card-img-container">
                                        <a href="<?php the_permalink(); ?>">
                                            <img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                                        </a>
                                    </div>

                                <?php endif ?>
                                <div class="card-body ">

                                    <div>
                                        <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h5>
                                        <div class="card-text"><?php the_advanced_excerpt(); ?>
                                        </div>

                                    </div>
                                    <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                                </div>
                            </div>

                        </div>
                    <?php endwhile; ?>
                <?php endif ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
        </div>
    </section>

<?php include('inc/compliance-footer-section.php'); ?>


<?php get_footer(); ?>