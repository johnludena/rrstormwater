<?php

  /* Register Navigation Menus
  =============================================== */
  register_nav_menu( 'primary-nav', __( 'Primary Nav' ) );
  register_nav_menu( 'utility-nav', __( 'Utility Nav' ) );
  register_nav_menu( 'footer-links', __( 'Footer Links' ) );
  
  /* Add custom search menu item to Utility Menu */  
  function add_search_form($items, $args) {
    if( $args->theme_location == 'utility-nav' )
            $items .= '<li>
              <form action="/" id="searchform" class="searchform expandable-search form-inline my-2 my-lg-0">
                <label for="searchform" class="sr-only">Search</label>
								<input class="form-control mr-sm-2" type="text" value="" name="s" id="s" placeholder="Search for...">
                <button type="submit">Search <i class="fas fa-search margin-left-10"></i></button>
                <i class="fas fa-times"></i>
							</form>
						</li>';
            return $items;
    } 

  add_filter('wp_nav_menu_items', 'add_search_form', 10, 2);

  /* Include Static Assets
  =============================================== */
  function include_static_assets() {

    // CSS files
    wp_enqueue_style('roboto', '//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap);', array(), 'all');
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/libs/bootstrap/css/bootstrap.min.css', array(), '4.3.1');
    wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/libs/font-awesome/css/all.css', array(), '5.9.0');
    wp_enqueue_style('twentytwenty-css', get_template_directory_uri() . '/libs/twentytwenty/css/twentytwenty.css', array(), 'all');
    wp_enqueue_style('royalslider-css', get_template_directory_uri() . '/libs/royalslider/royalslider.css', array(), 'all');
    wp_enqueue_style('royalslider-default-css', get_template_directory_uri() . '/libs/royalslider/skins/default/rs-default.css', array(), 'all');
    wp_enqueue_style('aos-css', '//cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css', array(), 'all');
    wp_enqueue_style('style-css', get_template_directory_uri() . '/style.css', array(), 'all');

    // JS Files
    // if( !is_page([36,39]) ) {
    //   wp_enqueue_script('jquery-lib-js', get_template_directory_uri() . '/assets/js/jquery-3.2.1.min.js', array(), '3.2.1');
    // }
    
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.min.js', array(), '4.3.1', true);
    wp_enqueue_script('jquery-event-move-js', get_template_directory_uri() . '/libs/twentytwenty/js/jquery.event.move.js', array(), 'all', true);
    wp_enqueue_script('twentytwenty-js', get_template_directory_uri() . '/libs/twentytwenty/js/jquery.twentytwenty.js', array(), 'all', true);
    wp_enqueue_script('sf-hoverintent-js', get_template_directory_uri() . '/libs/superfish/js/hoverIntent.js', array(), 'all', true);
    wp_enqueue_script('sf-superfish-js', get_template_directory_uri() . '/libs/superfish/js/superfish.min.js', array(), 'all', true);
    
    if( !is_front_page() ) {
      wp_enqueue_script('waypoints-js-old', get_template_directory_uri() . '/libs/jquery/jquery.waypoints.min-4.0.1.js', array(), null, true);
    }

    wp_enqueue_script('waypoints-js-new', get_template_directory_uri() . '/libs/jquery/jquery.waypoints.min-1.6.2.js', array(), null, true);
    wp_enqueue_script('counterup-js', get_template_directory_uri() . '/libs/jquery/jquery.counterup.js', array(), null, true);
    wp_enqueue_script('royalslider-js', get_template_directory_uri() . '/libs/royalslider/jquery.royalslider.min.js', array(), '9.5.9');
    wp_enqueue_script('aos-js', '//cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js', array(), 'all', true);
    wp_enqueue_script('sameszr-js', get_template_directory_uri() . '/libs/jquery-sameszr/js/jquery.samesizr.min.js', array(), null, true);
    wp_enqueue_script('scripts-js', get_template_directory_uri() . '/js/scripts.js', array(), 'all', true);

  }
  add_action('wp_enqueue_scripts', 'include_static_assets');

  /* Featured image support
  =============================================== */
  add_theme_support( 'post-thumbnails' );

  // Custom Excerpt function for Advanced Custom Fields
  function custom_field_excerpt() {
    global $post;
    $text = get_field('body_content'); //Replace 'your_field_name'
    if ( '' != $text ) {
      $text = strip_shortcodes( $text );
      $text = apply_filters('the_content', $text);
      $text = str_replace(']]>', ']]>', $text);
      $excerpt_length = 20; // 20 words
      $excerpt_more = apply_filters('excerpt_more', ' ' . '...');
      $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
    }
    return apply_filters('the_excerpt', $text);
  }


  /* Remove WordPress junk
  =============================================== */
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wp_generator');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'wp_shortlink_wp_head');

  /* Remove paragraphs around images & iframes
  =============================================== */
  function filter_ptags_on_images($strip_ptags){
     $strip_ptags = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $strip_ptags);
     $strip_ptags = preg_replace('/<p>\s*(<iframe.*>*.<\/iframe>)\s*<\/p>/iU', '\1', $strip_ptags);
     return $strip_ptags;
  }
  add_filter( 'the_content', 'filter_ptags_on_images' );

  /* Add SVG support to media uploader
  =============================================== */
  function custom_mtypes( $m ){
      $m['svg'] = 'image/svg+xml';
      $m['svgz'] = 'image/svg+xml';
      return $m;
  }
  add_filter( 'upload_mimes', 'custom_mtypes' );

  /* Obfuscate email addresses
  Use shotcode in editor: [mailto][/mailto]
  =============================================== */
  function cwc_mail_shortcode( $atts , $content=null ) {
    for ($i = 0; $i < strlen($content); $i++) $encodedmail .= "&#" . ord($content[$i]) . ';';
    return '<a href="mailto:'.$encodedmail.'">'.$encodedmail.'</a>';
  }
  add_shortcode('mailto', 'cwc_mail_shortcode');

  /* ACF options
  =============================================== */
  if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
    acf_add_options_sub_page('Telephone Number');
    acf_add_options_sub_page('Footer');
    acf_add_options_sub_page('404 Page');
    acf_add_options_sub_page('Top Navigation');
  }

  /* Truncate page & post content
  /* <?php echo content(30); ?> in a template
  =============================================== */
  function content($limit) {
    $content = apply_filters( 'the_content', get_the_content() );
    $content = strip_shortcodes(strip_tags($content, '<p>'));
    $content = explode(' ', $content, $limit);

    if (count($content)>=$limit) {
      array_pop($content);
      $content = implode(" ", $content).'...';
    } else {
      $content = implode(" ", $content).'...';
    }

    $content = preg_replace('/<img[^>]+./','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
  }

    // Strip blog post excerpt from headlines when showing preview content in card layout (h1 - h6)
    function wp_strip_header_tags( $excerpt='' ) {

        $raw_excerpt = $excerpt;
        if ( '' == $excerpt ) {
            $excerpt = get_the_content('');
            $excerpt = strip_shortcodes( $excerpt );
            $excerpt = apply_filters('the_content', $excerpt);
            $excerpt = str_replace(']]>', ']]&gt;', $excerpt);
        }

        $regex = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
        $excerpt = preg_replace($regex,'', $excerpt);

        $excerpt_length = apply_filters('excerpt_length', 55);
        $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
        $excerpt = wp_trim_words( $excerpt, $excerpt_length, $excerpt_more );

        return apply_filters('wp_trim_excerpt', preg_replace($regex,'', $excerpt), $raw_excerpt);
    }
    add_filter( 'get_the_excerpt', 'wp_strip_header_tags', 9);

  /* Login page logo replacement
  =============================================== */
  function namespace_login_headerurl( $url ) {
    $url = home_url( '/' );
    return $url;
  }
  add_filter( 'login_headertitle', 'namespace_login_headertitle' );

  function namespace_login_headertitle( $title ) {
    $title = get_bloginfo( 'name' );
    return $title;
  }
  add_action( 'login_head', 'namespace_login_style' );

  function namespace_login_style() {
    echo '<style>
      .login h1 a {
        height: 114px;
        width: 300px;
        margin: 0 auto 10px auto;
        padding: 0;
        background-image: url( ' . get_template_directory_uri() . '/images/logo-with-tag.svg);
        background-size: 80% auto;
        background-position: center center;
      }

      .login h1 a:focus {
        outline-width: 0;
        outline-style: none;
        outline-color: transparent;
        outline-offset: 0;
        box-shadow: none;
        text-decoration: none;
      }

      #nav {
        text-align: center;
      }

      #backtoblog {
        display: none;
      }
      </style>';
  }
  add_filter( 'login_headerurl', 'namespace_login_headerurl' );

  /* Add style sheet for administration area */
  /* =================================================== */
  function custom_admin_styles() {
    echo "<link type='text/css' rel='stylesheet' href='" . get_site_url() . "/wp-content/themes/stormwater/style-admin.css' />";
  }
  add_action('admin_head', 'custom_admin_styles');

  /* Add buttons that are disabled in Tiny MCE */
  /* =================================================== */
  function my_mce_buttons_3($buttons) {
    $buttons[] = 'superscript';
    $buttons[] = 'subscript';
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
  }
  add_filter( 'mce_buttons_3', 'my_mce_buttons_3' );
  
  add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );
  function my_mce_before_init($settings) {
    $style_formats = array(
      array(
        'title'         => 'Lead Paragraph',
        'selector'      => 'p',
        'classes'       => 'lead'
      ),
      array(
        'title'         => 'Large Text',
        'selector'      => '*',
        'classes'       => 'text-large'
      ),
      array(
        'title'         => 'Blue',
        'selector'      => '*',
        'classes'       => 'text-blue'
      ),
      array(
        'title'         => 'Red',
        'selector'      => '*',
        'classes'       => 'text-red'
      ),
      array(
        'title'         => 'White',
        'selector'      => '*',
        'classes'       => 'text-white'
      ),
      array(
        'title'         => 'Dark Grey',
        'selector'      => '*',
        'classes'       => 'text-dark'
      )
    );
    $settings['style_formats'] = json_encode( $style_formats );
    return $settings;
  }
  
?>