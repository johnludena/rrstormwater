<?php /* Template Name: Case Studies - Detail Page */ ?>
<?php get_header(); ?>
<?php while( have_posts() ) : the_post(); get_template_part( 'template-parts/content', 'page' ); if( comments_open() || get_comments_number() ) {
        comments_template();
} endwhile; ?>

<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
        <div class="row">
        <div class="col-lg-2">
            <?php include('inc/navigation-side.php'); ?>
        </div>

        
        <div class="col-10">
                        <div class="row">
                                <div class="col">
                                        <h1><?PHP echo get_the_title(); ?></h1>
                                        <p class="lead-paragraph"><?PHP echo get_field( "lead_paragraph" ); ?></p>
                                        <h2><?PHP echo get_field( "lead_image_title" ); ?></h2>
                                        <img src="<?PHP echo get_field('lead_image'); ?>" alt="">
                                        <p><?PHP echo get_field( "body_content" ); ?></p>
                                </div>
                        </div>
                        <div class="row addContList">
                        <?php

                        // check if the repeater field has rows of data
                        if( have_rows('image_card') ):

                                // loop through the rows of data
                            while ( have_rows('image_card') ) : the_row();
                                ?>


                                <div class="col-4">
                                        <img src="<?php the_sub_field('image_card_image'); ?>" alt="" class="img-fluid">
                                        <h3><?php the_sub_field('image_card_title'); ?></h3>
                                        <p><?php the_sub_field('image_card_description'); ?></p>
                                </div>


                                
                                
                                <?php 
                            endwhile;

                        else :

                            // no rows found

                        endif;

                        ?>

                                


                        </div>                  
                </div>
        </div>

</div>





</div><!-- .content-area -->


    <section id="compliance" class="padding-top-60 margin-top-100">
        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8">
                        <p class="text-white font-size-36"><strong><?php echo get_field('need_help_title'); ?></strong><br>
                        <?php echo get_field('need_help_description'); ?></p>
                    </div>
                    <div class="col-auto mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" href="<?php echo get_field('need_help_button_url'); ?>"><?php echo get_field('need_help_button_label'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>