<?php
/*
 * Template Name: Compliance
 * Template Post Type: compliance
*/

?>

<?php get_header(); ?>

    <section id="hero-banner">
        <div class="royalSlider royalSlider--hero rsDefault">

                        <div class="rsContent">
                            <img src="<?php the_field('banner_image'); ?>" />

                            <div class="slide-text">
                                <div class="inner-text-wrapper">
                                    <h1><?php the_field('banner_title'); ?></h1>
                                    <p><?php the_field('banner_description'); ?></p>
                                </div>

                            </div>
                        </div>
        </div>
    </section>

    <?php
                            
    $variable = get_field('compliance_title', 76);
    $variable = get_field('compliance_content', 76);
    $variable = get_field('compliance_button_label', 76);
                            
    ?>

    <section id="our-insights" class="padding-top-60">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">

                <h2><?php the_field('insights_title'); ?></h2>
                <hr/>
                <p><?php the_field('insights_intro_text'); ?></p><br/>

            </div>
        </div>
    </div>
</section>

<section id="our-insights" class="padding-top-60">
	<div class="container">
		<div class="row addContList">
	<?php

	        $query_posts = array(
				'post_type' 		=> 'post',
	            'posts_per_page' 	=> 6,
	            'orderby' 			=> 'date',
	            'order' 			=> 'DESC'
	          
			);
	 $insight_posts = new WP_Query( $query_posts ); ?>
	 <?php if ( $insight_posts->have_posts() ) :  ?>      
	 <?php while ( $insight_posts->have_posts() ) : $insight_posts->the_post(); ?>
<div class="col-lg-4 col-md-4 pb-3">
        <!-- <?php $image = get_field('blog_image'); if( !empty($image) ): ?><a href="<?php the_permalink(); ?>"> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="card-img-top js-eq-height-happenings-pic" /></a><?php else: ?> -->
        			<img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                    <section></section><?php endif ?>
				  	 <div class="card-body ">

				  		<div class="js-eq-height-happenings-store">
						  	<p class="card-type"><span class="bg-yellow">blog</span> <?php the_field('blog_type'); ?></p>
						    <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
						    <div class="card-text"><?php the_advanced_excerpt(); ?>
                            </div>

						</div>
					    <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
				  </div>
       
</div>
   <?php endwhile; ?>
 <?php endif ?>
<?php wp_reset_query(); ?>          
        </div>
    </div>
	 </div>
</section>


    <section id="compliance" class="padding-top-60 margin-bottom-60">
        <div class="container">
            <div class="row">
                <div class="col-6 justify-content-center">
                    <div class="card">
                        <img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                        <div class="card-heading card-body bg-gray-light">
                            <p class="card-category">Regulatory Compliance</p>
                            <h5 class="card-title"><?php the_field('compliance_title'); ?></h5>
                        </div>
                        <div class="card-body">
                            <?php the_field('compliance_content'); ?>
                            <a class="section-cta caret-right" href="#"><?php the_field('compliance_button_label'); ?></a>
                        </div>
                    </div>

                </div>
                <div class="col-6 justify-content-center">

                    <div id="featured" class="card h-100">
                        <?php
                        query_posts('posts_per_page=1');
                        if (have_posts()) while (have_posts()) : the_post(); ?>
                            <img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                            <div class="card-heading card-body bg-gray-light">
                                <p class="card-category">Featured Insight</p>
                                <h5 class="card-title"><?php the_title(); ?></h5>
<!--                                <p class="meta font-size-14">Morgan Ledford | 05/24/19</p>-->
                            </div>
                            <div class="card-body h-100">
                                <?php the_excerpt(); ?>
                                <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                            </div>

                        <?php endwhile; ?>

                        <?php wp_reset_query(); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8">
                        <p class="text-white font-size-36"><strong>Need help with a project?</strong><br>
                        Contact our team to get started today!</p>
                    </div>
                    <div class="col-auto mx-auto">
                        <a class="btn btn btn-outline-light btn-lg" href="#">Request a consultation</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>