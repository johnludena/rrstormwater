<?php
/*
 * Template Name: Expertise
 * Template Post Type: expertise
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<?php include('inc/hero-landing-banner.php'); ?>

<section id="our-expertise" class="padding-top-60">
	<div class="container">
		<div class="row addContList">
	<?php

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

<div class="col-lg-4 col-md-4 pb-3">
        <!-- <?php $image = get_field('blog_image'); if( !empty($image) ): ?><a href="<?php the_permalink(); ?>"> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="card-img-top js-eq-height-happenings-pic" /></a><?php else: ?> -->
        			<img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                    <section></section><?php endif ?>
				  	 <div class="card-body ">

				  		<div class="js-eq-height-happenings-store">
						    <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
						    <div class="card-text"><?php the_advanced_excerpt(); ?>
                            </div>

						</div>
					    <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
				  </div>
       
</div>
   <?php endwhile; ?>
 <?php endif ?>
<?php wp_reset_query(); ?>          
        </div>
    </div>
	 </div>
</section>


<?php include('inc/compliance-footer-section.php'); ?>

<?php get_footer(); ?>