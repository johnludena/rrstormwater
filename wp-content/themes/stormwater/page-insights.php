<?php
/*
 * Template Name: Insights
 * Template Post Type: insight
*/

?>

<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<?php include('inc/hero-landing-banner.php'); ?>

    <?php
                            
    $variable = get_field('compliance_title', 76);
    $variable = get_field('compliance_content', 76);
    $variable = get_field('compliance_button_label', 76);
                            
    ?>

<section id="our-insights" class="padding-top-bottom-60">
	<div class="container">
		<div class="row addContList">
<!-- BLOGS -->

                <?php
                global $post;
                $args = array( 'category' => '81',
                               'posts_per_page' => 1, 
                );
                $myposts = get_posts( $args );
                foreach( $myposts as $post ) :  setup_postdata($post); 
                ?>

                    <!-- CARD HTML -->
                    <div class="col-lg-4 col-md-4 pb-3">
                        <div class="card card--insights">
                            <?php
                            $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');

                            if( !empty($featured_img_url) ): ?>
                                <div class="card-img-container">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $featured_img_url; ?>" class="card-img-top" alt="" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="card-img-container">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                                    </a>
                                </div>

                            <?php endif ?>
                            <div class="card-body">

                                <div class="js-eqheight-card-body">
                                    <div class="insights-meta text-uppercase font-size-14 d-flex justify-content-between align-items-end margin-bottom-6">
                                        <span class="date text-gray-medium"><?php echo get_the_date(); ?></span>
                                        <span class="bg-blue-dark text-white padding-5 font-size-12 text-uppercase"><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></span>
                                    </div>
                                    <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

                                    <div class="card-text padding-top-10"><?php the_excerpt(); ?></div>

                                </div>
                                <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                            </div>
                        </div>

                    </div>

                <?php endforeach; ?>
                <?php wp_reset_query(); ?> 

<!-- NEWS -->
                <?php
                global $post;
                $args = array( 'category' => '82',
                               'posts_per_page' => 1, 
                );
                $myposts = get_posts( $args );
                foreach( $myposts as $post ) :  setup_postdata($post); 
                ?>

                    <!-- CARD HTML -->
                    <div class="col-lg-4 col-md-4 pb-3">
                        <div class="card card--insights">
                            <?php
                            $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');

                            if( !empty($featured_img_url) ): ?>
                                <div class="card-img-container">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $featured_img_url; ?>" class="card-img-top" alt="" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="card-img-container">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                                    </a>
                                </div>

                            <?php endif ?>
                            <div class="card-body">

                                <div class="js-eqheight-card-body">
                                    <div class="insights-meta text-uppercase font-size-14 d-flex justify-content-between align-items-end margin-bottom-6">
                                        <span class="date text-gray-medium"><?php echo get_the_date(); ?></span>
                                        <span class="bg-blue-dark text-white padding-5 font-size-12 text-uppercase"><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></span>
                                    </div>
                                    <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

                                    <div class="card-text padding-top-10"><?php the_excerpt(); ?></div>

                                </div>
                                <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                            </div>
                        </div>

                    </div>

                <?php endforeach; ?>
                <?php wp_reset_query(); ?> 


<!-- EVENTS -->

                <?php
                global $post;
                $args = array( 'category' => '83',
                               'posts_per_page' => 1, 
                );
                $myposts = get_posts( $args );
                foreach( $myposts as $post ) :  setup_postdata($post); 
                ?>

                    <!-- CARD HTML -->
                    <div class="col-lg-4 col-md-4 pb-3">
                        <div class="card card--insights">
                            <?php
                            $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');

                            if( !empty($featured_img_url) ): ?>
                                <div class="card-img-container">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $featured_img_url; ?>" class="card-img-top" alt="" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="card-img-container">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="https://picsum.photos/680/350" class="card-img-top" alt="...">
                                    </a>
                                </div>

                            <?php endif ?>
                            <div class="card-body">

                                <div class="js-eqheight-card-body">
                                    <div class="insights-meta text-uppercase font-size-14 d-flex justify-content-between align-items-end margin-bottom-6">
                                        <span class="date text-gray-medium"><?php echo get_the_date(); ?></span>
                                        <span class="bg-blue-dark text-white padding-5 font-size-12 text-uppercase"><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></span>
                                    </div>
                                    <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

                                    <div class="card-text padding-top-10"><?php the_excerpt(); ?></div>

                                </div>
                                <a class="section-cta caret-right" href="<?php the_permalink(); ?>">Read more</a>
                            </div>
                        </div>

                    </div>

                <?php endforeach; ?>
                <?php wp_reset_query(); ?> 



        </div>
    </div>
	 </div>
</section>

<?php include('inc/compliance-footer-section.php'); ?>

<?php get_footer(); ?>