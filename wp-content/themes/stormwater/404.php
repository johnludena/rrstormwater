<?php
/*
 * Template Name: 404 Page Not Found
 * Template Post Type: 404
*/

?>

<?php get_header(); ?>
<div id="breadcrumbContainer">

    <div class="container" >
        <div class="row">
            <div class="col">
                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </div>          
            </div>
        </div>
    </div>
    
</div>


<section id="our-case-studies" class="padding-top-60 padding-bottom-100">
	<div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-8">
                <h1 class="heading-bottom-border">It looks like this link needs restoration or rehabilitation.</h1>
            </div>
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-lg-8">
                <h2 class="padding-bottom-50">Error 404</h2>
                <h3>One of these pages may have what you're looking for:</h3>
                <br />
                <div class="row justify-content-center text-uppercase">
                    <div class="col-lg-6">
                        <ul id="sidenav" class="list-unstyled" role="navigation">
                            <div id="parent-<?php the_ID(); ?>" class="parent-page">
                                <li><a href="./services">Services</a></li>
                                <li><a href="./expertise">Expertise</a></li>
                                <li><a href="./compliance">Compliance</a></li>
                                <li><a href="./careers">Careers</a></li>
                                <li><a href="./contact-us">Contact Us</a></li>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>


<!--     <section id="compliance" class="padding-top-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 justify-content-center">

        <div class="waves-bg padding-top-60 padding-bottom-90">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8">
                        <p class="text-white font-size-36"><strong>Need help with a project?</strong><br>
                        Contact our team to get started today!</p>
                    </div>
                    <div class="col-auto">
                        <a class="btn btn btn-outline-light btn-lg" href="#">Request a consultation</a>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

<?php get_footer(); ?>