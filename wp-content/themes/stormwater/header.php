<!DOCTYPE html>
<html id="top" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php wp_title(); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/x-icon">

	<?php wp_head();?>

	<script type="text/javascript" src="https://adcetera.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-w83ft0/b/12/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=0ed2f797"></script>


</head>

<body <?php body_class(); ?> id="page-<?php global $post; $post_slug=$post->post_name; { echo $post_slug . ''; }?>">

<div class="fixed-top-nav-container fixed-top">
	<nav class="nav--navbar-utility-wrapper navbar navbar-expand-lg navbar-light">
		<div class="container">
			<a id="site-logo" href="<?php echo site_url(); ?>" title="<?php bloginfo('name'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/images/RR-Logo-Tagline.png" alt="R+R The Stormwater Management Co. logo">
			</a>
			<a id="site-logo-notag" href="<?php echo site_url(); ?>" title="<?php bloginfo('name'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/images/logo.svg" alt="R+R The Stormwater Management Co. logo">
			</a>

			<div class="navbar-collapse justify-content-end" id="navbarSupportedContent">
				<?php wp_nav_menu(
					array(
						'theme_location' => 'utility-nav',
						'menu_class' => 'navbar-nav'
					)
				); ?>

				<div class="contact-cta-container">
					<ul id="menu-contact-cta" class="navbar-nav align-items-center">
						<li><a href="tel:<?php the_field('telephone_number', 'option'); ?>"><?php the_field('telephone_number', 'option'); ?></a></li>
						<li><a class="btn btn-outline-primary" href="<?php echo get_home_url(); ?>/request-a-consultation">Free Consultation</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

	<div class="mobile-nav-actions bg-blue-dark">
		<a class="mobile-nav-trigger" href="#" title="Open menu">
			<div class="ham-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</a>

		<div class="mobile-nav-actions--icons d-flex justify-content-between">
			<a href="tel:<?php the_field('telephone_number', 'option'); ?>" title=""><i class="fas fa-phone"></i></a>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>client-login/" title=""><i class="fas fa-user-circle"></i></a>
		</div>
	</div>

	<nav class="nav--navbar-primary-wrapper navbar navbar-expand-lg navbar-dark">
		<div class="container justify-content-center">
			<?php wp_nav_menu(
				array(
					'theme_location' => 'primary-nav',
					'menu_class' => 'sf-menu navbar-nav mr-auto justify-content-around'
				)
			); ?>
			
			<div class="mobile-nav-utility">
				<?php wp_nav_menu(
					array(
						'theme_location' => 'utility-nav'
					)
				); ?>
			</div>
		</div>
	</nav>
</div>