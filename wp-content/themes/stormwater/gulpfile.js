var { series, parallel, src, dest, watch } = require('gulp');
var sass = require('gulp-sass');
var sassSourceMaps =  require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();

function compileSassOnUpdate() {
  console.log('Watching for changes in SASS files...');
  watch(['**/*.scss'], function () {

    return src('style.scss')
        .pipe(sassSourceMaps.init())
        .pipe(sass())
        .pipe(sassSourceMaps.write('.')) // write source maps to same directory as compiled CSS
        .pipe(dest('.'))
        .pipe(browserSync.stream())
  });
}

function startServer(done) {
  console.log('starting server...');

  browserSync.init({
    proxy: "http://localhost/rrstormwater"
  });

  done();
}


exports.default = series(startServer, compileSassOnUpdate);