<?php /* Template Name: Careers Dev - Detail Page */ ?>
<?php get_header(); ?>
<?php include('inc/breadcrumbs.php'); ?>

<div class="container" id="subpageContainer">
        <div class="row">
            <div class="col-lg-2">
                <?php include('inc/navigation-side.php'); ?>
            </div>
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        <h1><?php echo get_the_title(); ?></h1>
                        <p class="lead-paragraph"><?php echo get_field( "lead_paragraph" ); ?></p>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                        the_content();
                        endwhile; else: ?>
                            <p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
                    </div>
                </div>




            <div class="row addContList">
                <?php if( have_rows('image_card') ): while ( have_rows('image_card') ) : the_row(); ?>
                <div class="col-lg-4">
                    <img src="<?php the_sub_field('image_card_image'); ?>" alt="" class="img-fluid">
                    <h3><?php the_sub_field('image_card_title'); ?></h3>
                    <p><?php the_sub_field('image_card_description'); ?></p>
                </div>
                <?php endwhile; endif; ?>
            </div>                  
        </div>
    </div>
</div>


<?php include('inc/compliance-footer-without-section.php'); ?>

<?php get_footer(); ?>